# -*- coding: utf-8 -*-
"""
Overview
    Updates dashboard data
Usage
    Authenticate GCP account first:
        gcloud auth application-default login
        gcloud auth login
    python update_dashboard_data [campaign_code] [fw (optional)]
Author
    Andrew Lau
"""
import pandas as pd
from matplotlib import pyplot as plt
import seaborn as sns
import datetime
import os
import google.auth
from google.cloud import bigquery
from google.cloud import bigquery_storage
from google.cloud.exceptions import NotFound
import sys
import email_notifications
import logging

def update_dashboard_data(campaign_code, fw, bqclient=None, bqstorageclient=None):
    query_string = """
      -- delete old rows
    DELETE
    FROM
      `wx-bq-poc.personal.AL_FBAA_MC_dashboard_dacamp_prod_mc_final_crn_w_fb`
    WHERE
      campaign_code = "{campaign_code}"
      and week = "{fw}";
      --   add new rows
    INSERT INTO
      `wx-bq-poc.personal.AL_FBAA_MC_dashboard_dacamp_prod_mc_final_crn_w_fb`
    SELECT
      *,
      case
        when channel like "%_AlwaysOn_display%" then "_AlwaysOn_display"
        when channel like "%_AlwaysOn_google_other%" then "_AlwaysOn_google_other"
        when channel like "%_AlwaysOn_sem%" then "_AlwaysOn_sem"
        when channel like "%_AlwaysOn_video%" then "_AlwaysOn_video"
        when channel like "%_AlwaysOn_wow_web%" then "_AlwaysOn_wow_web"

        when channel like "%FB%" then "FB"
        when channel like "%IG%" then "IG"
        when channel like "%display%" then "display"
        when channel like "%email%" then "email"
        when channel like "%rw_app%" then "rw_app"
        when channel like "%rw_web_oap%" then "rw_web_oap"
        else "UNGROUPED"    
      end as channel_grouped    
    FROM
      `wx-bq-poc.personal.AL_FBAA_MC_dacamp_prod_mc_final_crn_w_fb_{fw}_{campaign_code}`
    WHERE
      campaign_code = "{campaign_code}";
    """.format(campaign_code=campaign_code, fw=fw)
    df_bq = (
        bqclient.query(query_string)
        .result()
        .to_dataframe(bqstorage_client=bqstorageclient)
    )
    
    logging.info("SUCCESS... updated table `wx-bq-poc.personal.AL_FBAA_MC_dashboard_dacamp_prod_mc_final_crn_w_fb`")
          
    return df_bq
          
if __name__ == "__main__":    
    # set user credentials again
    os.environ['GOOGLE_APPLICATION_CREDENTIALS']="adc.json" 

    # grab creentials from default login, use gcloud auth login
    credentials, your_project_id = google.auth.default(
        scopes=["https://www.googleapis.com/auth/cloud-platform"]
    )    
    
    # Make clients.
    bqclient = bigquery.Client(credentials=credentials, project='wx-bq-poc',)
    bqstorageclient = bigquery_storage.BigQueryReadClient(credentials=credentials)

    if len(sys.argv) == 3:  
        campaign_code = sys.argv[1]
        fw = sys.argv[2]    
    else:
        logging.error("invalid number of command line arguments. Usage: python run_FBAA_MC.py [campaign_code] [fw (optional)]")
        
    update_dashboard_data(campaign_code, fw, bqclient=bqclient, bqstorageclient=bqstorageclient)          
