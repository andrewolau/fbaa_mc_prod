# -*- coding: utf-8 -*-
"""
Overview
    Main program that runs the weekly social media attribution process.
Usage
    source activate ./MTA_AL
    
    # typical run - runs for the latest week
    python main.py
    
    # if doing a retro run or rerun
    python main.py [fw: eg 2021-06-14] [rerun FBAA flag: 'rerun' or "no"] [overwrite_backup_tables flag: 'overwrite' or 'no']
    
Author
    Andrew Lau
"""
from config import *
import backup_MTA_tables, FBAA_MC, downstream_process, update_mc_final_crn, gsheets, update_dashboard_data, email_notifications
import sys
import datetime
import os

import google.auth
from google.cloud import bigquery
from google.cloud import bigquery_storage
from google.cloud.exceptions import NotFound

import logging

##################################################
# LOGGING
##################################################
# logging.basicConfig(level=logging.DEBUG)
logFormatter = logging.Formatter("'%(asctime)s | %(filename)s | function: %(funcName)s | line: %(lineno)d : %(levelname)s - %(message)s'")
rootLogger = logging.getLogger()
rootLogger.setLevel(logging.DEBUG)  # need to set root logger to the lowest level


##################################################
# SETTING UP GCP CREDENTIALS AND BQ CLIENTS
##################################################
# set user credentials to create BQ client object
os.environ['GOOGLE_APPLICATION_CREDENTIALS']="adc.json" 
os.environ['GOOGLE_CLOUD_PROJECT']="wx-bq-poc" 

# grab creentials from default login, use gcloud auth login
credentials, your_project_id = google.auth.default(
    scopes=["https://www.googleapis.com/auth/cloud-platform"]
)

# make clients.
bqclient = bigquery.Client(credentials=credentials, project='wx-bq-poc',)
bqstorageclient = bigquery_storage.BigQueryReadClient(credentials=credentials)   

# set credentials to GCP service account to access sheets
os.environ['GOOGLE_APPLICATION_CREDENTIALS']="argo-dschapter-dev-c9b15ee5d63f.json"     

# reset user credentials to run BQ queries
os.environ['GOOGLE_APPLICATION_CREDENTIALS']="adc.json" 

fw = '2021-08-30'
# campaign_code = 'WCV-5070'
campaign_code = 'OSP-2373'



query_string = """
SELECT
    fw_start_date,
    campaign_code,
    sum(inc_sales) as inc_sales,
    count(*) as count
from
    `gcp-wow-rwds-ai-safari-prod.Attribution_Safari_GT_output.Safari_crn_level_game_theory_output_safari_full_promo_halo_post_current`
where fw_start_date = '{fw}' and campaign_code = '{campaign_code}' and pph = 'promo'
group by 1,2
;    
""".format(fw=fw, campaign_code=campaign_code)

results = (
    bqclient.query(query_string)
    .result()
    .to_dataframe(bqstorage_client=bqstorageclient)
)
logging.info("Check if campaign exists in GT output:")
logging.info(str(results))
campaign_exists_in_GT = not results.empty
logging.info(campaign_code + " exists in GT output: " + str(campaign_exists_in_GT))