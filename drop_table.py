# -*- coding: utf-8 -*-
"""
source activate MTA (this package has the drop tables functionality)

go to the below link FBAA > Uploaded > <YOUR TABLE> > upload history
https://business.facebook.com/advanced_analytics/query/v2?business_id=905253956232223&template_id=570898800197555


FBAA_TABLE_MAPPING = {0:"h_crn_seg_128",
                      1:"matrix_al_1",
                      2:"h_crn_seg_129"
#                       4:"h_crn_seg_137"  # 137 is not working
                     }

"""
import os
import fbaa.table_tools as table_tools

upload_session_to_drop = 2014540828715902

print(table_tools.drop_session(upload_session_to_drop))
