# -*- coding: utf-8 -*-
"""
Overview
    Backs up weekly MTA input tables
Usage
    python backup_MTA_tables.py ["overwrite" (optional)] [FW (optional, if you want to overwrite an older FW with the current intermediary MTA tables)]
    
    Authenticate GCP account first:
        gcloud auth application-default login
        gcloud auth login
Author
    Andrew Lau
"""
from config import *
import pandas as pd
import os
import google.auth
from google.cloud import bigquery
from google.cloud import bigquery_storage
from google.cloud.exceptions import NotFound
import datetime
import sys
import email_notifications
import time
import logging

def check_if_BQ_table_exists(table_id, bqclient=None, bqstorageclient=None):
    """
    Overview
        returns True or False depending on whether the table exists
    Arguments
        e.g. table_id = "wx-bq-poc.personal.AL_FBAA_MC_dacamp_prod_mc_final_crn_w_fb_2021-05-17_OSP-2373"
        NO tilde "`" surrounding table_id!
    """
    try:
        bqclient.get_table(table_id)  # Make an API request.
        logging.warning("Table {} already exists.".format(table_id))
        return True
    except NotFound:
        return False

def backup_MTA_tables(fw, overwrite_flag=False, bqclient=None, bqstorageclient=None):
    """
    Overview
        Backs up input source tables to the MTA process. These are tables that are overwritten every week.
        Run every week after the usual MTA process. Will not overwrite last week's tables.
    Arguments
        fw - financial week
        overwrite_flag - whether to overwrite any current backups
    Returns
        summary of backed up tables
    """
    if check_if_BQ_table_exists("wx-bq-poc.wx_lty_digital_attribution_dev.AL_FBAA_MC_dacamp_prod_event_mw_" + fw,
                               bqclient=bqclient, bqstorageclient=bqstorageclient):
        # if table already exists, then bail out
        logging.warning("Tables were already backed up for {0}".format(fw))
        if overwrite_flag:
            logging.info("Overwriting backed up tables for {0}".format(fw))
        else:
            logging.info("Exiting, no action taken.")
            return
    
    query_string = """
    -- backing up tables
    
    -- check that this is the right week
    ASSERT
    (select week from `gcp-wow-rwds-ai-safari-prod.digital_attribution_modelling.dacamp_prod_mc_final_crn` limit 1) = '{fw}'
    AS 'ERROR: backup weeks do not match!';
    
    create or replace table `wx-bq-poc.wx_lty_digital_attribution_dev.AL_FBAA_MC_dacamp_prod_event_mw_{fw}` as
    select * from `gcp-wow-rwds-ai-safari-prod.digital_attribution_modelling.dacamp_prod_event_mw`;

    create or replace table `wx-bq-poc.wx_lty_digital_attribution_dev.AL_FBAA_MC_dacamp_prod_activation_{fw}` as
    select * from `gcp-wow-rwds-ai-safari-prod.digital_attribution_modelling.dacamp_prod_activation`;

    create or replace table `wx-bq-poc.wx_lty_digital_attribution_dev.AL_FBAA_MC_dacamp_prod_online_sales_{fw}` as
    select * from `gcp-wow-rwds-ai-safari-prod.digital_attribution_modelling.dacamp_prod_online_sales`;

    create or replace table `wx-bq-poc.wx_lty_digital_attribution_dev.AL_FBAA_MC_dacamp_prod_mc_final_{fw}` as
    select * from `gcp-wow-rwds-ai-safari-prod.digital_attribution_modelling.dacamp_prod_mc_final`;

    create or replace table `wx-bq-poc.wx_lty_digital_attribution_dev.AL_FBAA_MC_dacamp_prod_mc_final_crn_{fw}` as
    select * from `gcp-wow-rwds-ai-safari-prod.digital_attribution_modelling.dacamp_prod_mc_final_crn`;

    create or replace table `wx-bq-poc.wx_lty_digital_attribution_dev.AL_FBAA_MC_dacamp_prod_mc_final_crn_latest` as
    select * from `gcp-wow-rwds-ai-safari-prod.digital_attribution_modelling.dacamp_prod_mc_final_crn`;

    create or replace table `wx-bq-poc.wx_lty_digital_attribution_dev.AL_FBAA_MC_marketable_crn_{fw}` as
    select * from `gcp-wow-rwds-ai-safari-prod.digital_attribution_modelling.marketable_crn`;

    create or replace table `wx-bq-poc.wx_lty_digital_attribution_dev.AL_FBAA_MC_dacamp_prod_mc_union_fb_{fw}` as
    select * from `gcp-wow-rwds-ai-safari-prod.digital_attribution_modelling.dacamp_prod_mc_union_fb`;

    create or replace table `wx-bq-poc.digital_attribution_modelling.tmp_customer_encrypted_detail` as
    select * from `gcp-wow-rwds-ai-safari-prod.wdp_tables.customer_encrypted_detail`;

    -- Xueyuan's token is being used and doesn't have access to the safari prod tables, making temp copy
    create or replace table `wx-bq-poc.personal.AL_Safari_crn_level_game_theory_output_safari_full_promo_halo_post_current_big_w` as
    select * from `gcp-wow-rwds-ai-safari-prod.Attribution_Safari_GT_output.Safari_crn_level_game_theory_output_safari_full_promo_halo_post_current_big_w`;

    create or replace table `wx-bq-poc.personal.AL_Safari_crn_level_game_theory_output_safari_full_promo_halo_post_current` as
    select * from `gcp-wow-rwds-ai-safari-prod.Attribution_Safari_GT_output.Safari_crn_level_game_theory_output_safari_full_promo_halo_post_current`;

    create or replace table `wx-bq-poc.personal.AL_ttl_crn_level_store` as
    select * from `gcp-wow-rwds-ai-safari-prod.Attribution_Safari_Matching.ttl_crn_level_store`;

    create or replace table `wx-bq-poc.personal.AL_ttl_crn_level_store_big_w` as
    select * from `gcp-wow-rwds-ai-safari-prod.Attribution_Safari_Matching.ttl_crn_level_store_big_w`;
    
    -- summary to check if tables were made
    select "dacamp_prod_event_mw" as table, count(*) as count from `wx-bq-poc.wx_lty_digital_attribution_dev.AL_FBAA_MC_dacamp_prod_event_mw_{fw}`
    UNION ALL
    select "dacamp_prod_activation" as table, count(*) as count from `wx-bq-poc.wx_lty_digital_attribution_dev.AL_FBAA_MC_dacamp_prod_activation_{fw}`
    UNION ALL
    select "dacamp_prod_online_sales" as table, count(*) as count from `wx-bq-poc.wx_lty_digital_attribution_dev.AL_FBAA_MC_dacamp_prod_online_sales_{fw}`
    UNION ALL
    select "dacamp_prod_mc_final" as table, count(*) as count from `wx-bq-poc.wx_lty_digital_attribution_dev.AL_FBAA_MC_dacamp_prod_mc_final_{fw}`
    UNION ALL
    select "dacamp_prod_mc_final_crn" as table, count(*) as count from `wx-bq-poc.wx_lty_digital_attribution_dev.AL_FBAA_MC_dacamp_prod_mc_final_crn_{fw}`
    UNION ALL
    select "marketable_crn" as table, count(*) as count from `wx-bq-poc.wx_lty_digital_attribution_dev.AL_FBAA_MC_marketable_crn_{fw}`
    UNION ALL
    select "dacamp_prod_mc_union_fb" as table, count(*) as count from `wx-bq-poc.wx_lty_digital_attribution_dev.AL_FBAA_MC_dacamp_prod_mc_union_fb_{fw}`
    ;    
    """.format(fw=fw)
    logging.info("""
    BQ row count check should look something like:
    0	dacamp_prod_mc_final	400
    1	dacamp_prod_online_sales	300,000
    2	dacamp_prod_mc_final_crn	6m
    3	dacamp_prod_activation	5m
    4	dacamp_prod_event_mw	28m
    5	marketable_crn	10m
    """)
    results = (
        bqclient.query(query_string)
        .result()
        .to_dataframe(bqstorage_client=bqstorageclient)
    )
    return results


# start backup run here
def backup_this_week(fw=None, overwrite_flag=False, bqclient=None, bqstorageclient=None):
    # if no FW provided, then use latest FW
    if fw is None:
        today = datetime.date.today()
        last_monday = today + datetime.timedelta(days=-today.weekday(), weeks=-1)
        fw = str(last_monday)
    
    try:
        results = backup_MTA_tables(fw, overwrite_flag=overwrite_flag, bqclient=bqclient, bqstorageclient=bqstorageclient)        

        if results is None:  # e.g. if table already exists, the function returns None
            message_body = "Table was already backed up at a previous date, nothing was done."
        else:
            logging.info(str(results))
            
            message_body = """
            Tables created:

            `wx-bq-poc.wx_lty_digital_attribution_dev.AL_FBAA_MC_dacamp_prod_event_mw_{fw}`
            `wx-bq-poc.wx_lty_digital_attribution_dev.AL_FBAA_MC_dacamp_prod_activation_{fw}`
            `wx-bq-poc.wx_lty_digital_attribution_dev.AL_FBAA_MC_dacamp_prod_online_sales_{fw}`
            `wx-bq-poc.wx_lty_digital_attribution_dev.AL_FBAA_MC_dacamp_prod_mc_final_{fw}`
            `wx-bq-poc.wx_lty_digital_attribution_dev.AL_FBAA_MC_dacamp_prod_mc_final_crn_{fw}`
            `wx-bq-poc.wx_lty_digital_attribution_dev.AL_FBAA_MC_marketable_crn_{fw}`

            Row counts of tables backed up:
            {results_table}

            BQ row count check should look something like:
            0	dacamp_prod_mc_final	400
            1	dacamp_prod_online_sales	300,000
            2	dacamp_prod_mc_final_crn	6m
            3	dacamp_prod_activation	5m
            4	dacamp_prod_event_mw	28m
            5	marketable_crn	10m
            """.format(results_table=str(results), fw=fw)
        
        email_notifications.send_email_notification(email_recipient=EMAIL_RECIPIENT_ALL,
                                                    subject='wow.bot🤖: 💾✔️ BACKUP of MTA input tables for fw ' +  str(fw),
                                                    message_body=message_body)
    except Exception as ex:  # e-mail if error happens        
        time.sleep(5)  # sleep to prevent gmail from blocking rapid message sending
        email_notifications.send_email_notification(email_recipient=EMAIL_RECIPIENT_ALL,
                                                    subject='wow.bot🤖: 💾❌ FAILED... BACKUP of MTA input tables for fw ' +  str(fw),
                                                    message_body="ERROR:" + str(type(ex)) + str(ex) + str(ex.args))

if __name__ == "__main__":
    rootLogger = logging.getLogger()
    rootLogger.setLevel(logging.INFO) 

    # set user credentials again
    os.environ['GOOGLE_APPLICATION_CREDENTIALS']="adc.json"
    
    # grab creentials from default login, use gcloud auth login
    credentials, your_project_id = google.auth.default(
        scopes=["https://www.googleapis.com/auth/cloud-platform"]
    )
    
    # Make clients.
    bqclient = bigquery.Client(credentials=credentials, project='wx-bq-poc',)
    bqstorageclient = bigquery_storage.BigQueryReadClient(credentials=credentials)   

    # run the process
    overwrite_flag = False
    specify_fw_flag = False
    if len(sys.argv) == 2:  # ONE command line argument - overwrite
        if sys.argv[1] == "overwrite":
            overwrite_flag = True
        else:
            logging.error("Invalid command line argument")

    elif len(sys.argv) == 3:  # TWO command line argument - overwrite and FW
        logging.info("running for specific FW")
        if sys.argv[1] == "overwrite":
            overwrite_flag = True
        else:
            logging.error("Invalid command line argument")
        specify_fw_flag = True
        fw = sys.argv[2]

    if specify_fw_flag:
        logging.info("running for specific FW" + fw)
        backup_MTA_tables(fw=fw, overwrite_flag=overwrite_flag, bqclient=bqclient, bqstorageclient=bqstorageclient)
    else:
        backup_this_week(overwrite_flag=overwrite_flag, bqclient=bqclient, bqstorageclient=bqstorageclient)
