# -*- coding: utf-8 -*-
"""
Config file for social media attribution
"""
# whether to update the final tables, mc_final_crn, for Weixing (normally True for live runs)
UPDATE_FINAL_TABLES = False

# 0, 1 or 2. Use another table is one is hanging
FB_TABLE_NUM = 0

FBAA_TABLE_MAPPING = {0:"h_crn_seg_128",
                      1:"matrix_al_1",
                      2:"h_crn_seg_129"
#                       4:"h_crn_seg_137"  # 137 is not working
                     }

# new conversion and touchpoints definition
NEW_DEFINITION = False

# who gets the email notifications
EMAIL_RECIPIENT_ALL = 'alau3@woolworths.com.au', 'wxu@woolworths.com.au' # recipients email notifications, separated by commas

EMAIL_RECIPIENT = 'alau3@woolworths.com.au'
