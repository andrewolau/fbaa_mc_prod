#!bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin:/opt/miniconda/bin:/opt/conda/condabin:/opt/conda/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/local/jre1.8.0_201/bin:/usr/local/jre1.8.0_201/bin
echo "03_run_fbaa_mc.sh: running"
# usage: bash 01_run_fbaa_mc.sh [fw (optional)]
# run "gcloud auth application-default login" to authenticate first

source activate ./MTA_AL

echo "03_run_fbaa_mc.sh: git pull"
git pull origin master

echo "03_run_fbaa_mc.sh: autorun.py"
python autorun.py || true  # continue even if this fails so log gets pushed to repo

echo "03_run_fbaa_mc.sh: git push"
git add .
git commit -am "latest weeks results"
git push origin master
