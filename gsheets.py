# -*- coding: utf-8 -*-
"""
Usage: python gsheets.py [fw]

Add this account to gsheets sharing permissions: argo-service-account@gcp-wow-rwds-ai-dschapter-dev.iam.gserviceaccount.com
Authenticate with GCP service account first:
    gcloud auth activate-service-account argo-service-account@gcp-wow-rwds-ai-dschapter-dev.iam.gserviceaccount.com --key-file=argo-dschapter-dev-c9b15ee5d63f.json
    export GOOGLE_APPLICATION_CREDENTIALS=argo-dschapter-dev-c9b15ee5d63f.json
References: https://developers.google.com/sheets/api/quickstart/python
"""
from __future__ import print_function
import os.path
import os
import sys
import logging

from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials

import google.auth
from google.cloud import bigquery
from google.cloud import bigquery_storage
from google.cloud.exceptions import NotFound

import pandas as pd

# If modifying these scopes, delete the file token.json.
# SCOPES = ['https://www.googleapis.com/auth/spreadsheets.readonly']

# The ID and range of a sample spreadsheet.
# the below is a test case
# SAMPLE_SPREADSHEET_ID = '1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms'
# SAMPLE_RANGE_NAME = 'Class Data!A2:E'

SAMPLE_SPREADSHEET_ID = '1yWEmhDZvu_pWXl0DXkJ84ARHVSE_nIvoR7vs4V0OJDM'  # grab from URL
SAMPLE_RANGE_NAME = 'Social campaigns for MTA!A1:H'

def is_this_campaign_TTL(campaign_code, bqclient=None, bqstorageclient=None, verbose=False):
    """
    Overview
        Returns True if the campaign code is a TTL campaign according to `gcp-wow-rwds-ai-safari-prod.wdp_tables.safarievents_final`    
    Arguments
        campaign_code - campaign code, string, eg "CVM-1661"
        bqclient - BQ client object. Allows passing through of authenticated BQ objects.
            If no BQ objects are provided then one will be created.
        bqstorageclient - BQ client storage object
    Returns
        Boolean indicating if a campaign is TTL or not
        Returns None if that campaign isn't in Wilson's data
    Examples
    >>> is_this_campaign_TTL("CVM-1661", bqclient, bqstorageclient, verbose=False)
    True
    >>> is_this_campaign_TTL("OSP-2373", bqclient, bqstorageclient, verbose=False)
    False
    >>> is_this_campaign_TTL("MADE_UP_CAMPAIGN", bqclient, bqstorageclient, verbose=False)
    WARNING: campaign MADE_UP_CAMPAIGN not found in `gcp-wow-rwds-ai-safari-prod.wdp_tables.safarievents_final`, assuming it is NOT TTL
    False
    >>> is_this_campaign_TTL("WCV-4697", bqclient, bqstorageclient, verbose=False)
    False
    """
    if bqclient is None:  # if a BQ client object is not passed through to the function
        # grab credentials from default login, use gcloud auth login
        credentials, your_project_id = google.auth.default(
            scopes=["https://www.googleapis.com/auth/cloud-platform"]
        )

        # make clients
        bqclient = bigquery.Client(credentials=credentials, project='wx-bq-poc',)
        bqstorageclient = bigquery_storage.BigQueryReadClient(credentials=credentials)   
    
    query_string= """
    SELECT
      campaign_code,
      MAX(campaign_ttl) AS campaign_ttl
    FROM
      `gcp-wow-rwds-ai-safari-prod.wdp_tables.safarievents_final`
    WHERE campaign_code = '{campaign_code}'
    GROUP BY
      1
    ORDER BY
      1;
    """.format(campaign_code=campaign_code)
    df = (bqclient.query(query_string).result().to_dataframe(bqstorage_client=bqstorageclient))
    
    if df.shape[0] == 0:  # if BQ returns an empty DF
        logging.error("campaign " + campaign_code + " not found in `gcp-wow-rwds-ai-safari-prod.wdp_tables.safarievents_final`")
        # return none if campaign not found
        result = None
    else:  # campaign exists in Wilson's data
        if df.campaign_ttl[0] is None:  # if campaign is in Wilson's data, but TTL flag is none, just hardcode to True
            result = False  # BTL
        else:
            result = df.campaign_ttl[0]
    
    if verbose:        
        logging.info(campaign_code + " is a TTL campaign... " + str(result))
    
    return result

def extract_from_gsheets(fw, bqclient, bqstorageclient, verbose=False):
    """
    For a given fw, extracts campaign codes, FB campaign IDs and TTL flags for the FB MTA runs
    
    Examples    
    >>> extract_from_gsheets("2021-07-05", bqclient, bqstorageclient, verbose=False)
    {'OSP-2373': {'campaign_ID': '23848236572270307,23848236326320307,23848236288380307,23848236258460307,23848236221030307,23848236019080307,23848236314010307,23848293531080307,23848236276600307,23848432397450307,23848432862940307,23848360959260307,23848302301450307,23848302286890307,23848302278980307,23848302205400307,23848302144050307,23848294019370307', 'TTL_flag': False, 'division': 'supers'}}
    
    >>> extract_from_gsheets("2021-05-17", bqclient, bqstorageclient, verbose=False)
    {'OSP-2373': {'campaign_ID': '6239361258831', 'TTL_flag': False, 'division': 'supers'}}
    
    >>> extract_from_gsheets("2021-07-12", bqclient, bqstorageclient, verbose=False)
    {'OSP-2373': {'campaign_ID': '23848236572270307,23848236326320307,23848236288380307,23848236258460307,23848236221030307,23848236019080307,23848236314010307,23848293531080307,23848236276600307,23848432397450307,23848432862940307,23848360959260307,23848302301450307,23848302286890307,23848302278980307,23848302205400307,23848302144050307,23848294019370307', 'TTL_flag': False, 'division': 'supers'}, 'WCV-4697': {'campaign_ID': '6246870264431', 'TTL_flag': False, 'division': 'big_w'}}
    """
    # build interface with gsheets
    service = build('sheets', 'v4'
#                     , credentials=creds
                   )

    # Call the Sheets API
    sheet = service.spreadsheets()
    result = sheet.values().get(spreadsheetId=SAMPLE_SPREADSHEET_ID,
                                range=SAMPLE_RANGE_NAME).execute()
    values = result.get('values', [])

    # save input as a df
    df = pd.DataFrame(values[1:])
    df.columns = values[0]
    if verbose:
        print("All data:")
        print(df)
    
    # extract relevant fw
    df_this_fw = df[df.FW == fw]
    if verbose:
        print("Just this FW:")    
        print(df_this_fw)
    
    # turn into dictionary
    campaign_mapping = dict()
    
    for index, row in df_this_fw.iterrows():
        campaign_code_cleaned = row["CAMPAIGN CODE"].strip().replace(" ", "").replace("  ", "").replace("   ", "").replace("    ", "").replace("\n", "")  # strip whitespace

        # order in which to process the campaigns
        if campaign_code_cleaned in ("OSP-2373"):
            order = 1
        elif campaign_code_cleaned in ("WCT-5002"):
            order = 2
        else:
            order = 3

        campaign_mapping[campaign_code_cleaned] = {"campaign_ID":row["CAMPAIGN ID"].strip().replace(" ", "").replace("  ", "").replace("   ", "").replace("    ", "").replace("\n", ""),  # strip whitespace
                                                  "TTL_flag":is_this_campaign_TTL(campaign_code_cleaned, bqclient, bqstorageclient, verbose=verbose),
                                                  "division":row["DIVISION"],
                                                  "order":order}

    campaign_mapping_sorted = {k: v for k, v in sorted(campaign_mapping.items(), key=lambda item: item[1]["order"])}
        
    if verbose:
        print("Campaign mapping from gsheets:", campaign_mapping)
        print("campaign_mapping_sorted", campaign_mapping_sorted)
    
    return campaign_mapping_sorted

if __name__ == '__main__':
    # set user credentials to create BQ object
    os.environ['GOOGLE_APPLICATION_CREDENTIALS']="adc.json"
    
    # grab creentials from default login, use gcloud auth login
    credentials, your_project_id = google.auth.default(
        scopes=["https://www.googleapis.com/auth/cloud-platform"]
    )
    
    # Make clients.
    bqclient = bigquery.Client(credentials=credentials, project='wx-bq-poc',)
    bqstorageclient = bigquery_storage.BigQueryReadClient(credentials=credentials)   

    # set credentials to GCP service account to access gsheets
    os.environ['GOOGLE_APPLICATION_CREDENTIALS']="argo-dschapter-dev-c9b15ee5d63f.json"

    # 1 command line argument - campaign ID, fw is the latest week
    if len(sys.argv) == 2:
        if sys.argv[1] == "test":
            import doctest
            logging.info("running doctests")
            pd.set_option('display.max_rows', 500)
            pd.set_option('display.max_columns', 500)
            pd.set_option('display.width', 1000)
            
            print("Running doctests")
            doctest.testmod(optionflags=doctest.NORMALIZE_WHITESPACE)
            print("Doctests complete")
            exit()
        else:
            fw = sys.argv[1]
    
    extract_from_gsheets("2021-09-13", bqclient, bqstorageclient, verbose=True)  # run for test week    
    

