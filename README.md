# README  
![image info](safari_fbaa_logo.png)  

Please see the Confluence page:  
https://woolworthsdigital.atlassian.net/wiki/spaces/PAR/pages/2135721632/Social+Media+Attribution  

### What is this repository for? ###

This is the Productionised Social Media Attribution process.

**What it does**

1. Backs up MTA input tables
2. Pulls data from google sheets provided by social media team
3. Runs FB MTA
4. Runs downstream processes (CRN level allocation) and updates final output table `mc_final_crn` that is passed along to downstream processes
6. Updates data for the Social Media Attribution Dashboard
7. Pushes the results back to the repo, saving run logs

### How do I get set up? ###

**Summary of set up**  

1. Git clone and run `00_create_env.sh`
2. Run `01_copy_sql_scripts.sh`.   

**Configuration**  

* Add your email to the variable EMAIL_RECIPIENTS and EMAIL_RECIPIENTS_ALL to `config.py`
* If the FBAA upload table is choked/hanging you can use use another FBAA upload in `config.py`

**Usage**  

For a manual run use `bash 04_run_fbaa_mc_manual.sh`.  

* For a typical run - runs for the latest week: `bash 04_run_fbaa_mc_manual.sh`  
* For a retro run/rerun: `bash 04_run_fbaa_mc_manual.sh [fw: eg 2021-06-14] [rerun FBAA flag: 'rerun' or "no"] [overwrite_backup_tables flag: 'overwrite' or 'no']`  

**Deployment instructions**  

Schedule a cron job to check every 15 minutes Tuesday to Wednesday to see if the Safari tables have been updated. `03_run_fbaa_mc.sh` will leave markers to ensure that the process is only autorun once per week. If it fails, for whater reason, you will need to manually run it or remove the autorun marker.  

`crontab -u andrewlau -e`  

Then edit your cron tab:  

`*/15 2-13 * * 2-5 cd ~/fbaa_mc_prod; bash 03_run_fbaa_mc.sh > logs/cron.log 2>&1`

### Who do I talk to? ###

* Repo owner or admin  
Andrew Lau
* Other community or team contact  
Weixing, James Sorrell, Xueyuan Dong