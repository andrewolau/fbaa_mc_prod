# -*- coding: utf-8 -*-
"""
Overview
    Main program that runs the weekly social media attribution process.
Usage
    source activate ./MTA_AL
    
    # typical run - runs for the latest week
    python main.py
    
    # if doing a retro run or rerun
    python main.py [fw: eg 2021-06-14] [rerun FBAA flag: 'rerun' or "no"] [overwrite_backup_tables flag: 'overwrite' or 'no']
    
Author
    Andrew Lau
"""
from config import *
import backup_MTA_tables, FBAA_MC, downstream_process, update_mc_final_crn, gsheets, update_dashboard_data, email_notifications
import sys
import datetime
import os

import google.auth
from google.cloud import bigquery
from google.cloud import bigquery_storage
from google.cloud.exceptions import NotFound

import logging
##################################################
# LOGGING
##################################################
# logging.basicConfig(level=logging.DEBUG)
logFormatter = logging.Formatter("'%(asctime)s | %(filename)s | function: %(funcName)s | line: %(lineno)d : %(levelname)s - %(message)s'")
rootLogger = logging.getLogger()
rootLogger.setLevel(logging.DEBUG)  # need to set root logger to the lowest level

now = datetime.datetime.now() + datetime.timedelta(hours=10)  # convert to Sydney time
now = now.strftime("%d-%m-%Y_%H%M")
fileHandler = logging.FileHandler("{0}/{1}.log".format("logs", now), 'w+')
fileHandler.setLevel(logging.DEBUG)
fileHandler.setFormatter(logFormatter)
rootLogger.addHandler(fileHandler)

consoleHandler = logging.StreamHandler()
consoleHandler.setLevel(logging.INFO)
consoleHandler.setFormatter(logFormatter)
rootLogger.addHandler(consoleHandler)

# catch all exceptions in the log
# def my_handler(type, value, tb):
#     rootLogger.exception("Uncaught exception: {0}".format(str(value)))

# # Install exception handler
# sys.excepthook = my_handler

# log config
logging.info("UPDATE_FINAL_TABLES = {0}".format(UPDATE_FINAL_TABLES))
logging.info("FB_TABLE_NUM = {0}".format(FB_TABLE_NUM))
logging.info("FBAA_TABLE_MAPPING = {0}".format(FBAA_TABLE_MAPPING))
logging.info("NEW_DEFINITION = {0}".format(NEW_DEFINITION))
logging.info("EMAIL_RECIPIENT_ALL = {0}".format(EMAIL_RECIPIENT_ALL))
logging.info("EMAIL_RECIPIENT = {0}".format(EMAIL_RECIPIENT))

##################################################
# COMMAND LINE INPUTS
##################################################
# defaults (not command line args)
rerun = False
overwrite_backup_tables = False

# NO command line arguments - run for latest fw
if len(sys.argv) == 1:  
    today = datetime.date.today()
    last_monday = today + datetime.timedelta(days=-today.weekday(), weeks=-1)
    fw = str(last_monday)    

# 1 command line argument - specify fw
elif len(sys.argv) == 2:  
    fw = sys.argv[1]

# 2 command line arguments - specify fw and rerun flag
elif len(sys.argv) == 3:
    fw = sys.argv[1]
    if sys.argv[2] == 'rerun':
        rerun = True
        logging.info("rerunning FBAA")

# 3 command line arguments - specify fw, rerun flag and overwrite backup flag
elif len(sys.argv) == 4:
    fw = sys.argv[1]
    if sys.argv[2] == 'rerun':
        rerun = True
        logging.info("rerunning FBAA")
    if sys.argv[3] == 'overwrite':
        overwrite_backup_tables = True
        logging.info("overwrite_backup_tables is TRUE")
else:
    logging.error("Invalid number of command line arguments.")
    exit()

# get END of fw
datetime_object = datetime.datetime.strptime(fw, '%Y-%m-%d')
datetime_object_plus_6_days = datetime_object + datetime.timedelta(days=6)
fw_end = datetime_object_plus_6_days.strftime("%Y-%m-%d")

##################################################
# SETTING UP GCP CREDENTIALS AND BQ CLIENTS
##################################################
# set user credentials to create BQ client object
os.environ['GOOGLE_APPLICATION_CREDENTIALS']="adc.json" 
os.environ['GOOGLE_CLOUD_PROJECT']="wx-bq-poc" 

# grab creentials from default login, use gcloud auth login
credentials, your_project_id = google.auth.default(
    scopes=["https://www.googleapis.com/auth/cloud-platform"]
)

# make clients.
bqclient = bigquery.Client(credentials=credentials, project='wx-bq-poc',)
bqstorageclient = bigquery_storage.BigQueryReadClient(credentials=credentials)   

# set credentials to GCP service account to access sheets
os.environ['GOOGLE_APPLICATION_CREDENTIALS']="argo-dschapter-dev-c9b15ee5d63f.json"     

# grab campaigns codes and IDs to run from google sheets
fb_run_dict = gsheets.extract_from_gsheets(fw,  bqclient, bqstorageclient)

# reset user credentials to run BQ queries
os.environ['GOOGLE_APPLICATION_CREDENTIALS']="adc.json" 

##################################################
# RUN MC FBAA PROCESS
##################################################
# backup tables
# if backup:
backup_MTA_tables.backup_this_week(fw=fw, overwrite_flag=overwrite_backup_tables, bqclient=bqclient, bqstorageclient=bqstorageclient)

# leaving run marker for this fw
logging.debug("creating fw run marker")
try:
    os.makedirs("logs/fw_run_marker")
except FileExistsError:
    # directory already exists
    pass
f = open("logs/fw_run_marker/" + fw + ".txt", "w")
# MAIN LOOP
for index, campaign_code in enumerate(fb_run_dict):
    if (fw == '2021-09-06' and campaign_code != 'WCT-5002') or (fw == '2021-12-20' and campaign_code == 'OSP-2373'):
        print("skipping", campaign_code)
        continue

    logging.info("starting FBAA_MC.run_MTA_FB_w_email for {0}, {1}".format(campaign_code, fb_run_dict[campaign_code]))

    # if the campaign code is not in Wilson's Safari events data
    if fb_run_dict[campaign_code]["TTL_flag"] is None:
        if campaign_code == "CVM-3509":
            fb_run_dict[campaign_code]["TTL_flag"] = False
            logging.warning(campaign_code + " is not in Wilson's Safari events data, BUT continuing due to exception...")
        else:
            logging.error(campaign_code + " is not in Wilson's Safari events data, skipping...")
            email_notifications.send_email_notification(email_recipient=EMAIL_RECIPIENT,
                                                subject='wow.bot🤖: 🔎❌ WARNING CAMPAIGN NOT FOUND IN SAFARI EVENTS, SKIPPING: ' +  fw + ": " + campaign_code,
                                                message_body="")
            continue

    FBAA_MC.run_MTA_FB_w_email(campaign_code=campaign_code,
                               fw=fw,
                               campaign_id=fb_run_dict[campaign_code]["campaign_ID"],
                               fb_table_num=FB_TABLE_NUM,
                               rerun=rerun,
                               ttl_flag=fb_run_dict[campaign_code]["TTL_flag"],
                               division=fb_run_dict[campaign_code]["division"],
                               debug=False
                              )
    logging.info("finished FBAA_MC.run_MTA_FB_w_email for {0}, {1}".format(campaign_code, fb_run_dict[campaign_code]))

    # if FBAA upload failed, skip to next campaign code
    if not os.path.isfile(campaign_code + "/res_w_fb_weighted_" + campaign_code + "_" + fw + "_" + fw_end + "_7.csv"):
        logging.warning("FBAA MC part failed, moving to next campaign code")
        continue
    
    # run downstream process
    logging.info("starting run_downstream_process for {0}, {1}".format(campaign_code, fb_run_dict[campaign_code]))
    downstream_process.run_downstream_process(campaign_code=campaign_code, fw=fw, bqclient=bqclient, bqstorageclient=bqstorageclient,
                                              ttl_flag=fb_run_dict[campaign_code]["TTL_flag"],
                                              division=fb_run_dict[campaign_code]["division"])   
    logging.info("finished run_downstream_process for {0}, {1}".format(campaign_code, fb_run_dict[campaign_code]))
    
    # update data for dash: https://datastudio.google.com/s/jXHlbvp8Gk4    
    update_dashboard_data.update_dashboard_data(campaign_code, fw, bqclient=bqclient, bqstorageclient=bqstorageclient)
    logging.info("dashboard data update complete")

    # update the final table
    logging.info("index" + str(index))
    logging.info("len(fb_run_dict)" + str(len(fb_run_dict)))
    if index + 1 == len(fb_run_dict):  # if last campaign, send out email to ALL email recipients        
        logging.info("index + 1 == len(fb_run_dict), sending email to EMAIL_RECIPIENTS_ALL")
        send_email = True
    else:
        send_email = False
    if UPDATE_FINAL_TABLES:
        logging.info("starting update_mc_final_crn_w_email")
        update_mc_final_crn.update_mc_final_crn_w_email(campaign_code=campaign_code, fw=fw, send_email=send_email, bqclient=bqclient, bqstorageclient=bqstorageclient)
        logging.info("finished update_mc_final_crn_w_email")
