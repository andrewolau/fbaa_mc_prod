#!bin/bash
# echo $PATH
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin:/opt/miniconda/bin:/opt/conda/condabin:/opt/conda/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/local/jre1.8.0_201/bin:/usr/local/jre1.8.0_201/bin

# typical run - runs for the latest week
# bash 04_run_fbaa_mc_manual.sh
    
# if doing a retro run or rerun
# bash 04_run_fbaa_mc_manual.sh [fw: eg 2021-06-14] [rerun FBAA flag: 'rerun' or "no"] [overwrite_backup_tables flag: 'overwrite' or 'no']

echo "04_run_fbaa_mc_manual.sh: running"
# usage: bash 01_run_fbaa_mc.sh [fw (optional)]
# run "gcloud auth application-default login" to authenticate first

source activate ./MTA_AL

echo "04_run_fbaa_mc_manual.sh: git pull"
git pull

echo "04_run_fbaa_mc_manual.sh: main.py"
python main.py $@ || true  # continue even if this fails so log gets pushed to repo

echo "04_run_fbaa_mc_manual.sh: git push"
git add .
git commit -am "latest weeks results"
git push
