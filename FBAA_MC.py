# -*- coding: utf-8 -*-
"""
Overview
    Weekly FBAA MC run
Usage
    python FBAA_MC.py [campaign_code] [fw (optional)] [FB campaign_id (optional)] [table number: 1, 2, 3, 4 (optional)]
Author
    Andrew Lau
"""
from config import *
import sys
import mta
import datetime
import pandas as pd
from fbaa.fb_interface import FBAttributionManger
from fbaa.sql_templates import CampaignConversionLogic, CampaignEventsLogic
from mta import Timer, ca_from_journey
import pickle
import time
import os
import email_notifications
import logging

def run_mta_FB(campaign_start, campaign_code, campaign_id=None, fb_table_num=1, rerun=True, ttl_flag=False, division="supers", debug=False):
    prefix = "w_fb_weighted_"
    
    # output file paths
    dirname = os.path.dirname(__file__)
    fpath = os.path.join(dirname, campaign_code + "/")
    
    datetime_object = datetime.datetime.strptime(campaign_start, '%Y-%m-%d')
    logging.info("check https://business.facebook.com/adsmanager for latest fb_campaign_id")
    
    if campaign_id:
        fb_campaign_id = campaign_id    
    else:
        logging.error("invalid campaign_code")
        return
    
    # campaign end date 6 days later
    datetime_object = datetime.datetime.strptime(campaign_start, '%Y-%m-%d')
    datetime_object_plus_6_days = datetime_object + datetime.timedelta(days=6)
    campaign_end = datetime_object_plus_6_days.strftime("%Y-%m-%d")  

    
    fb_touch_days = 7
    
    if division == "supers":
        if ttl_flag:
            conversion_file = "conversion_qty_TTL.sql"
        else:  # BTL
            conversion_file = "conversion_qty_BTL.sql"
    elif division == "big_w":
        if ttl_flag:
            conversion_file = "conversion_qty_TTL_big_w.sql"
        else:
            conversion_file = "conversion_qty_BTL_big_w.sql"
    event_file = "campaign_events_shanglin.sql"
           
    # changed to new conversion and touchpoints file    
    if NEW_DEFINITION:  # new definition just from the event_mw file
        conversion_file = "conversion_qty_new.sql"  # conversions from events_mw
        event_file = "campaign_events_shanglin_filtered.sql"  # events_mw filtered for inc sales not null

    logging.info("using SQL conversion file{0}".format(conversion_file))
    logging.info("using SQL event file{0}".format(event_file))

    new_fbaa_table = FBAA_TABLE_MAPPING[fb_table_num] 

    logging.info("using FBAA table {0}".format(new_fbaa_table))
    
#     template_transmatrix = 2869284200067641  # original
    template_transmatrix = 184549573529263  # IG support
    macro_value = "macros_transmatrix_292428505673909"
    table_str = "wx-bq-poc.xueyuan.AL_FBAA_MC_MTA_package_output"
    verbose_flag = debug

    suffix = prefix + campaign_code + "_" + campaign_start + "_" + campaign_end + "_" + str(fb_touch_days)
    
    # exit if run already
    if os.path.exists(fpath + "res_" + suffix + ".csv")    :
        logging.warning("FBAA MC already run for,{0}res_{1}.csv".format(fpath, suffix))
        if rerun:
            logging.info("rerunning")
        else:
            logging.info("Exiting FBAA MC...")
            return
    
    logging.info(">>> Step 1: running MTA pipeline")
    now = datetime.datetime.now()
    start_time = time.time()

    btl_logic = CampaignEventsLogic(
        event_file=event_file,
        save_head=True,
        campaign_code_1=campaign_code,
        campaign_start_date_1=campaign_start,
        campaign_end_date_1=campaign_end,
    )

    conversion_logic = CampaignConversionLogic(
        conversion_file=conversion_file,
        save_head=True,
        campaign_code_1=campaign_code,
        campaign_start_date_1=campaign_start,
        campaign_end_date_1=campaign_end,
    )

    fbm = FBAttributionManger(
        conversion_sql_logic=conversion_logic,
        events_sql_logic=btl_logic,
        event_start_date=campaign_start,
        days=fb_touch_days,
        table_str=table_str,
        template_transmatrix=template_transmatrix,
        date_partition=campaign_start,
        new_fbaa_table=new_fbaa_table,
        curr_campaign=fb_campaign_id,
        macro_value=macro_value,
        verbose=verbose_flag
    ) 
        
#     email_notifications.send_email_notification(email_recipient=EMAIL_RECIPIENT,
#                                                 subject='wow.bot🤖: STARTING MTA with FB run for fw ' +  campaign_start + ": " + campaign_code)
    logging.info("fbm.run_pipeline():")
    attribution = fbm.run_pipeline()

    logging.info(attribution)
    logging.info(">>> Step 2: saving output")
    # save results (this is the live, current one used for all downstream processes)
    pd.DataFrame(attribution).to_csv(fpath + "res_" + suffix + ".csv")    
    # save backup of results
    pd.DataFrame(attribution).to_csv(fpath + "res_" + suffix + "_"+ now.strftime("%Y-%m-%d_%H%M") + ".csv")
    
    logging.info(">>> Step 3: generating diagnostics")
    # save customer journey
    fbm.df_cj.to_csv(fpath + "cj_" + suffix + ".csv")
    # save backup of results
    fbm.df_cj.to_csv(fpath + "cj_" + suffix + "_"+ now.strftime("%Y-%m-%d_%H%M") + ".csv")
    
    # save CA tranmatrix
    tran_matrix_data = mta.ca_generate_transition_matrix(fbm.df_cj)
    file = open(fpath + "tran_matrix_data_" + suffix+ ".pickle", 'wb')
    pickle.dump(tran_matrix_data, file)
    file.close()

    # getting tran matrix
    tran_matrix = tran_matrix_data["transition_matrix"]

    # getting channel mapping
    channels = tran_matrix_data["channels"]
    channels = channels.set_index("id_channel")
    channel_dict = channels.to_dict()["channel_name"]
    channel_dict["(start)"] = "(start)"
    channel_dict["(null)"] = "(null)"
    channel_dict["(conversion)"] = "(conversion)"
    channel_dict_str = dict()
    for key in channel_dict:
        channel_dict_str[str(key)] = channel_dict[key]
    channel_dict_str

    # mapping channel id to channel name
    tran_matrix.loc[:, "channel_from"] = tran_matrix.channel_from.map(channel_dict_str)
    tran_matrix.loc[:, "channel_to"] = tran_matrix.channel_to.map(channel_dict_str)
    tran_matrix.to_csv(fpath + "tran_matrix_" + suffix + ".csv")

    end_time = time.time()
    total_time = (end_time - start_time) // 60
    
    logging.info(">>> Step 4: email notifications")
    message_body = """
    Total runtime: {total_time} minutes
    FB campaign ID:{fb_campaign_id}
    Attribution results:
    {results_table}
    """.format(results_table=str(pd.DataFrame(attribution)), total_time=total_time, fb_campaign_id=fb_campaign_id)
        
    email_notifications.send_email_notification(email_recipient=EMAIL_RECIPIENT,
                                                subject='wow.bot🤖:📊✔️ FINISHED MTA with FB run for fw ' +  campaign_start + ": " + campaign_code,
                                                message_body=message_body)

def run_MTA_FB_w_email(campaign_code, fw=None, campaign_id=None, fb_table_num=1, rerun=True, ttl_flag=False, division="supers", debug=False):
    # make directory for that campaign, if does not exist
    if not os.path.exists(campaign_code):
        logging.info("First run for this campaign, creating a directory")
        os.makedirs(campaign_code)
        os.makedirs(campaign_code + "/comparison")
    
    # if no fw is provided, use the latest week
    if fw is None:
        today = datetime.date.today()
        last_monday = today + datetime.timedelta(days=-today.weekday(), weeks=-1)
        fw = str(last_monday)
    logging.info("campaign code is {0} fw is {1}".format(campaign_code, fw))
    
    # if this was already run, skip
    try:
        run_mta_FB(fw, campaign_code, campaign_id, fb_table_num=fb_table_num, rerun=rerun, ttl_flag=ttl_flag, division=division, debug=debug)
    except Exception as ex:  # e-mail if error happens
        logging.error(str(type(ex)))
        logging.error(str(ex))
        logging.error(str(ex.args))
        
        time.sleep(5)  # sleep to prevent gmail from blocking rapid message sending
        email_notifications.send_email_notification(email_recipient=EMAIL_RECIPIENT,
                                                    subject='wow.bot🤖: ❌ FAILED... MTA with FB run for fw ' +  fw + ": " + campaign_code,
                                                    message_body="ERROR:" + str(type(ex)) + str(ex) + str(ex.args))

if __name__ == "__main__":
    fw = None  # None as sentinel for latest week
    campaign_id = None
    fb_table_num = 1
    # 1 command line argument - campaign code, fw is the latest week
    if len(sys.argv) == 2:  
        campaign_code = sys.argv[1]
    # 2 command line arguments - specify both campaign code and fw
    elif len(sys.argv) == 3:
        campaign_code = sys.argv[1]
        fw = sys.argv[2]  # None as sentinel for latest week
    # 3 command line arguments - specify campaign code, fw and campaign_id (from FB side)
    elif len(sys.argv) == 4:
        campaign_code = sys.argv[1]
        fw = sys.argv[2]
        campaign_id = sys.argv[3]
    # 4 command line arguments - specify campaign code, fw and campaign_id (from FB side) and table id
    elif len(sys.argv) == 5:
        campaign_code = sys.argv[1]
        fw = sys.argv[2]
        campaign_id = sys.argv[3]
        fb_table_num = sys.argv[4]
    else:
        logging.error("Invalid number of command line arguments. Usage: python FBAA_MC.py [campaign_code] [fw (optional)] [FB campaign_id (optional)] [table number: 1, 2, 3, 4 (optional)]")    

    run_MTA_FB_w_email(campaign_code=campaign_code,
                       fw=fw,
                       campaign_id=campaign_id,
                       fb_table_num=fb_table_num)
    