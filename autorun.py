# -*- coding: utf-8 -*-
"""
Overview
    Auto run, checks if the final MTA tables have been updated and if so, kicks off the social attribution pipeline
Usage
    Must use the MTA_AL environment installed in this directory.
    
    source activate ./MTA_AL
    
    schedule with a cron job
Author
    Andrew Lau
"""
from config import *
import pandas as pd
import os
import google.auth
from google.cloud import bigquery
from google.cloud import bigquery_storage
from google.cloud.exceptions import NotFound
import datetime
import sys
import email_notifications
import time
import logging

def was_mta_table_updated_today(bqclient=None, bqstorageclient=None, verbose=False):
    """
    Overview
        checks if gcp-wow-rwds-ai-safari-prod.digital_attribution_modelling.dacamp_prod_mc_final_crn was updated today    
    Returns
        Tuple consisting of
            Boolean indicated whether gcp-wow-rwds-ai-safari-prod.digital_attribution_modelling.dacamp_prod_mc_final_crn was updated today
            Dataframe with the final MTA table's details
        
    """
    if bqclient is None:  # if bqclients not provided, create one
        # grab creentials from default login, use gcloud auth login
        credentials, your_project_id = google.auth.default(
            scopes=["https://www.googleapis.com/auth/cloud-platform"]
        )
        # Make clients.
        bqclient = bigquery.Client(credentials=credentials, project='wx-bq-poc',)
        bqstorageclient = bigquery_storage.BigQueryReadClient(credentials=credentials)   

    query_string = """
    SELECT
      *,
      DATETIME(creation_time, 'Australia/Sydney') as creation_time_AEST
    FROM
      gcp-wow-rwds-ai-safari-prod.digital_attribution_modelling.INFORMATION_SCHEMA.TABLES
    WHERE
      table_name = "dacamp_prod_mc_final_crn" ;
    """

    results = (
        bqclient.query(query_string)
        .result()
        .to_dataframe(bqstorage_client=bqstorageclient)
    )
    today = datetime.date.today()
    
    if verbose:
        print(results)
        print("gcp-wow-rwds-ai-safari-prod.digital_attribution_modelling.dacamp_prod_mc_final_crn last updated", str(results.creation_time_AEST[0]))
        print("today is", today)
        created_today = results.creation_time_AEST[0] >= pd.Timestamp(today)
        print("created_today... results.creation_time_AEST[0] >= today =", created_today)
    
    return created_today, results

if __name__ == "__main__":
    print("AUTORUN: checking if final MTA tables were run today...")
    # set user credentials again
    os.environ['GOOGLE_APPLICATION_CREDENTIALS']="adc.json"
    
    # grab creentials from default login, use gcloud auth login
    credentials, your_project_id = google.auth.default(
        scopes=["https://www.googleapis.com/auth/cloud-platform"]
    )
    
    # create latest fw variable
    today = datetime.date.today()
    last_monday = today + datetime.timedelta(days=-today.weekday(), weeks=-1)
    fw = str(last_monday)    
    
    # Make clients.
    bqclient = bigquery.Client(credentials=credentials, project='wx-bq-poc',)
    bqstorageclient = bigquery_storage.BigQueryReadClient(credentials=credentials)   
            
    # check if final MTA table was updated today
    run_today, table_update_details = was_mta_table_updated_today(bqclient=bqclient, bqstorageclient=bqstorageclient, verbose=True)
    
    # if the MTA was run today and autorun did not run already for this fw
    if run_today and (not os.path.exists("logs/fw_run_marker/" + fw + ".txt")):
        print("MTA run today... kicking off Social Media Attribution")
        email_notifications.send_email_notification(email_recipient=EMAIL_RECIPIENT_ALL,
                                                    subject='wow.bot🤖: Final MTA table update detected, kicking off Social Media Attribution pipeline',
                                                    message_body=str(table_update_details))
            
        import main
    else:
        print("AUTORUN: MTA not run today, exiting...")
