# -*- coding: utf-8 -*-
"""
Overview
    Runs post MTA downstream process - allocation to CRN level, application of business logic and
    upload to BQ
Usage
    python downstream_process.py [campaign_code] [fw (optional)]
    
    Authenticate GCP account first:
    gcloud auth application-default login
    gcloud auth login
Author
    Andrew Lau
"""
from config import *
import pandas as pd
from matplotlib import pyplot as plt
import seaborn as sns
import datetime
import os
import google.auth
from google.cloud import bigquery
from google.cloud import bigquery_storage
import sys
import email_notifications
import logging

def convert_to_post_MTA_format(fpath_results,
                               campaign_code,
                               fw,
                               dacamp_prod_mc_final="`wx-bq-poc.personal.AL_FBAA_MC_dacamp_prod_mc_final`",
                               rm_fb=True,
                               create_dacamp_prod_mc_final=False,
                               bqclient=None,
                               bqstorageclient=None,
                               division="supers"):
    """
    Overview
        Convert output from MTA package into post MC format in Safari (for downstream processing to MC CRN level).
        Saves to BQ at the location dacamp_prod_mc_final (an argument)
    Arguments
        fpath_results - filepath to CSV with channel attribution, MTA package output
        campaign_code - e.g. "CVM-1661"
        fw - financial week, e.g. "2021-04-12"
        dacamp_prod_mc_final - where to save MC attribution in post MTA format, BQ table
        rm_fb=False - whether to remove FB touchpoints from the results DF before uploading to BQ
    Returns
        table saved to BQ
    Examples
    >>> convert_to_post_MTA_format(fpath_results="OSP-2373/res_w_fb_weighted_OSP-2373_2021-06-28_2021-07-04_7.csv", campaign_code = 'OSP-2373', fw='2021-06-28', dacamp_prod_mc_final="`wx-bq-poc.personal.AL_FBAA_MC_dacamp_prod_mc_final_2021-07-05_OSP-2373`", create_dacamp_prod_mc_final=False, bqclient=bqclient, bqstorageclient=bqstorageclient)
        removing facebook touchpoints from MC output
        df generated for upload to BQ at `wx-bq-poc.personal.AL_FBAA_MC_mc_final_tmp`
        post MTA output uploaded to BQ
                                                      key                       event  channel_prob_norm
    0   Total_supermarkets_OSP-2373_2021-06-23_2021-06-29         CVM-1661_email_open           0.000067
    1   Total_supermarkets_OSP-2373_2021-06-23_2021-06-29         CVM-1661_rw_app_imp           0.003706
    2   Total_supermarkets_OSP-2373_2021-06-23_2021-06-29          OSP-2373_email_clk           0.002556
    3   Total_supermarkets_OSP-2373_2021-06-23_2021-06-29         OSP-2373_email_open           0.013592
    4   Total_supermarkets_OSP-2373_2021-06-23_2021-06-29         OSP-2373_rw_app_clk           0.026948
    5   Total_supermarkets_OSP-2373_2021-06-23_2021-06-29         OSP-2373_rw_app_imp           0.163975
    6   Total_supermarkets_OSP-2373_2021-06-23_2021-06-29     OSP-2373_rw_web_oap_clk           0.001219
    7   Total_supermarkets_OSP-2373_2021-06-23_2021-06-29  _AlwaysOn_sem_shopping_clk           0.000118
    8   Total_supermarkets_OSP-2373_2021-06-23_2021-06-29       _AlwaysOn_wow_web_clk           0.014160
    9   Total_supermarkets_OSP-2373_2021-06-23_2021-06-29      _AlwaysOn_wow_web_view           0.019672
    10  Total_supermarkets_OSP-2373_2021-06-30_2021-07-04          CVM-1661_email_clk           0.000116
    11  Total_supermarkets_OSP-2373_2021-06-30_2021-07-04         CVM-1661_email_open           0.000452
    12  Total_supermarkets_OSP-2373_2021-06-30_2021-07-04         CVM-1661_rw_app_imp           0.004041
    13  Total_supermarkets_OSP-2373_2021-06-30_2021-07-04          OSP-2373_email_clk           0.283398
    14  Total_supermarkets_OSP-2373_2021-06-30_2021-07-04         OSP-2373_email_open           0.242352
    15  Total_supermarkets_OSP-2373_2021-06-30_2021-07-04         OSP-2373_rw_app_clk           0.041852
    16  Total_supermarkets_OSP-2373_2021-06-30_2021-07-04         OSP-2373_rw_app_imp           0.135703
    17  Total_supermarkets_OSP-2373_2021-06-30_2021-07-04     OSP-2373_rw_web_oap_clk           0.000926
    18  Total_supermarkets_OSP-2373_2021-06-30_2021-07-04  _AlwaysOn_sem_shopping_clk           0.000141
    19  Total_supermarkets_OSP-2373_2021-06-30_2021-07-04       _AlwaysOn_wow_web_clk           0.024134
    20  Total_supermarkets_OSP-2373_2021-06-30_2021-07-04      _AlwaysOn_wow_web_view           0.020872    
    >>> convert_to_post_MTA_format(fpath_results="OSP-2373/res_w_fb_weighted_OSP-2373_2021-07-05_2021-07-11_7.csv", campaign_code = 'OSP-2373', fw='2021-07-05', dacamp_prod_mc_final="`wx-bq-poc.personal.AL_FBAA_MC_dacamp_prod_mc_final_2021-07-05_OSP-2373`", create_dacamp_prod_mc_final=False, bqclient=bqclient, bqstorageclient=bqstorageclient) 
        removing facebook touchpoints from MC output
        df generated for upload to BQ at `wx-bq-poc.personal.AL_FBAA_MC_mc_final_tmp`
        post MTA output uploaded to BQ
                                                      key                       event  channel_prob_norm
    0   Total_supermarkets_OSP-2373_2021-06-30_2021-07-06         CVM-1661_email_open           0.000256
    1   Total_supermarkets_OSP-2373_2021-06-30_2021-07-06         CVM-1661_rw_app_imp           0.001782
    2   Total_supermarkets_OSP-2373_2021-06-30_2021-07-06          OSP-2373_email_clk           0.001982
    3   Total_supermarkets_OSP-2373_2021-06-30_2021-07-06         OSP-2373_email_open           0.013784
    4   Total_supermarkets_OSP-2373_2021-06-30_2021-07-06         OSP-2373_rw_app_clk           0.028692
    5   Total_supermarkets_OSP-2373_2021-06-30_2021-07-06         OSP-2373_rw_app_imp           0.206936
    6   Total_supermarkets_OSP-2373_2021-06-30_2021-07-06     OSP-2373_rw_web_oap_clk           0.000935
    7   Total_supermarkets_OSP-2373_2021-06-30_2021-07-06     _AlwaysOn_sem_brand_clk           0.000120
    8   Total_supermarkets_OSP-2373_2021-06-30_2021-07-06  _AlwaysOn_sem_shopping_clk           0.000834
    9   Total_supermarkets_OSP-2373_2021-06-30_2021-07-06       _AlwaysOn_wow_web_clk           0.014081
    10  Total_supermarkets_OSP-2373_2021-06-30_2021-07-06      _AlwaysOn_wow_web_view           0.023801
    11  Total_supermarkets_OSP-2373_2021-07-07_2021-07-11         CVM-1661_rw_app_imp           0.002275
    12  Total_supermarkets_OSP-2373_2021-07-07_2021-07-11          OSP-2373_email_clk           0.191654
    13  Total_supermarkets_OSP-2373_2021-07-07_2021-07-11         OSP-2373_email_open           0.211915
    14  Total_supermarkets_OSP-2373_2021-07-07_2021-07-11         OSP-2373_rw_app_clk           0.061099
    15  Total_supermarkets_OSP-2373_2021-07-07_2021-07-11         OSP-2373_rw_app_imp           0.170635
    16  Total_supermarkets_OSP-2373_2021-07-07_2021-07-11     OSP-2373_rw_web_oap_clk           0.001782
    17  Total_supermarkets_OSP-2373_2021-07-07_2021-07-11     _AlwaysOn_sem_brand_clk           0.000087
    18  Total_supermarkets_OSP-2373_2021-07-07_2021-07-11  _AlwaysOn_sem_shopping_clk           0.000239
    19  Total_supermarkets_OSP-2373_2021-07-07_2021-07-11       _AlwaysOn_wow_web_clk           0.038693
    20  Total_supermarkets_OSP-2373_2021-07-07_2021-07-11      _AlwaysOn_wow_web_view           0.028421
    """
    # import saved FBAA MC results from CSV
    df = pd.read_csv(fpath_results)
    
    # rm fb touchpoints and rescale everything back up to 100%
    if rm_fb:
        logging.info("removing facebook touchpoints from MC output")
        mask_fb = df.channel_name.str.startswith('FB_')
        mask_ig = df.channel_name.str.contains('Instagram')

        logging.info("mask_fb contains")
        logging.info(df[mask_fb])

        logging.info("mask_ig contains")
        logging.info(str(df[mask_ig]))
        
#         conversion_value_fb = df.loc[(df.channel_name == "FB_Click") | (df.channel_name == "FB_Impression") | (df.channel_name == "FB_Impression_MobileFeed") | (df.channel_name == "FB_Impression_InstagramStory") | (df.channel_name == "FB_Impression_WebFeed") | (df.channel_name == "FB_Impression_Instagram") | (df.channel_name == "FB_Click_MobileFeed") | (df.channel_name == "FB_Impression_SuggestedVideo"), "normalised_conversion_value"].sum()
        
#         conversion_value_non_fb = df.loc[(df.channel_name != "FB_Click") & (df.channel_name != "FB_Impression") & (df.channel_name != "FB_Impression_MobileFeed") & (df.channel_name != "FB_Impression_InstagramStory") & (df.channel_name != "FB_Impression_WebFeed") & (df.channel_name != "FB_Impression_Instagram") & (df.channel_name != "FB_Click_MobileFeed") & (df.channel_name != "FB_Impression_SuggestedVideo"), "normalised_conversion_value"].sum()

        conversion_value_fb = df.loc[mask_fb].normalised_conversion_value.sum()        
        conversion_value_non_fb = df.loc[~mask_fb].normalised_conversion_value.sum()        

        # sum check
        if abs(conversion_value_fb + conversion_value_non_fb) - 1 >= 0.0001:
            logging.info("abs(conversion_value_fb + conversion_value_non_fb) - 1 >= 0.000")
            logging.info(str(abs(conversion_value_fb + conversion_value_non_fb) - 1)            )
        assert abs(conversion_value_fb + conversion_value_non_fb) - 1 <= 0.0001
        
        # rescale non FB touchpoints back to 100%
        scaling = 1 / conversion_value_non_fb
        df.loc[:, "normalised_conversion_value"] = scaling * df.loc[:, "normalised_conversion_value"]
        
        # dropping FB touchpoints
#         df = df[(df.channel_name != "FB_Click") & (df.channel_name != "FB_Impression") & (df.channel_name != "FB_Impression_MobileFeed") & (df.channel_name != "FB_Impression_InstagramStory") & (df.channel_name != "FB_Impression_WebFeed") & (df.channel_name != "FB_Impression_Instagram") & (df.channel_name != "FB_Click_MobileFeed") & (df.channel_name != "FB_Impression_SuggestedVideo")]
        df = df[~mask_fb]
    
    df = df.drop("Unnamed: 0", axis="columns")

    df.loc[:, "event"] = df.channel_name.str.extract(r"(.*)_(\d{4}-\d{2}-\d{2})_(\d{4}-\d{2}-\d{2})").iloc[:, 0]
    df.loc[:, "campaign_start_date"] = df.channel_name.str.extract(r"(.*)_(\d{4}-\d{2}-\d{2})_(\d{4}-\d{2}-\d{2})").iloc[:, 1]
    df.loc[:, "campaign_end_date"] = df.channel_name.str.extract(r"(.*)_(\d{4}-\d{2}-\d{2})_(\d{4}-\d{2}-\d{2})").iloc[:, 2]

    # extract channel, medium, event from full MC node name
    event_extract = df.channel_name.str.extract(r"({campaign_code}|CVM-1661|_AlwaysOn)_(display|email|rw_app|google_other|sem|wow_web|rw_web_oap|video)(_brand|_generic|_other|_shopping)?_(clk|open|imp|view|push_clk|push_imp)".format(campaign_code=campaign_code)).fillna("")
    event_extract

    df.loc[:, "channel"] = event_extract.iloc[:, 0] + "_" + event_extract.iloc[:, 1]
    df.loc[:, "medium"] = event_extract.iloc[:, 0] + "_" + event_extract.iloc[:, 1] + event_extract.iloc[:, 2]
   
    if division == "supers":
        df.loc[:, "key"] = "Total_supermarkets_" + campaign_code + "_" + df.loc[:, "campaign_start_date"] + '_' + df.loc[:, "campaign_end_date"]
        df.loc[:, "banner"] = "supermarkets"
    elif division in ("bigw", "big_w"):
        df.loc[:, "key"] = "Total_bigw_" + campaign_code + "_" + df.loc[:, "campaign_start_date"] + '_' + df.loc[:, "campaign_end_date"]
        df.loc[:, "banner"] = "bigw"
    else:
        logging.error("division not in ('supers', 'bigw', 'big_w')")
        
    df.loc[:, "campaign_code"] = campaign_code
    df = df.rename({"normalised_conversion_value":"channel_prob_norm"}, axis="columns")
    
#     return df
    # check extraction went OK
    if df.isnull().sum().sum() != 0:
        logging.error("input dataframe has NANs... exiting")
        logging.info(str(df))
        return df
    assert df.isnull().sum().sum() == 0
    
    # if there are channels that don't belong here
    if df[(df.channel == "_") | (df.medium == "_")].shape[0] > 0:
        conversion_amount_dropped = df[(df.channel == "_") | (df.medium == "_")].channel_prob_norm.sum()
        logging.warning("dropping the below rows from dacamp_prod_mc_final with conversion value of:" + str(conversion_amount_dropped))
        logging.warning(r"channel_name extraction failed using regex expression: ({campaign_code}|CVM-1661|_AlwaysOn)_(display|email|rw_app|google_other|sem|wow_web|rw_web_oap|video)(_brand|_generic|_other|_shopping)?_(clk|open|imp|view|push_clk|push_imp)")
        logging.warning(str(df[(df.channel == "_") | (df.medium == "_")]))
        assert df[(df.channel == "_") | (df.medium == "_")].channel_prob_norm.sum() < 0.05
        df = df[((df.channel != "_") & (df.medium != "_"))]
    
    logging.info("df generated for upload to BQ at `wx-bq-poc.personal.AL_FBAA_MC_mc_final_tmp`")

    # dump to BQ for downstream processing (convert to CRN level)
    df.to_gbq('personal.AL_FBAA_MC_mc_final_tmp', project_id='wx-bq-poc', if_exists='replace')
    
    if create_dacamp_prod_mc_final:
        comment_out = "#"
    else:
        comment_out = ""

    query_string = """
    -- generating dummy table
    # create or replace table {dacamp_prod_mc_final} as
    create or replace table `wx-bq-poc.personal.AL_FBAA_MC_mc_final_tmp_2` as
    
    {comment_out}select * from `wx-bq-poc.wx_lty_digital_attribution_dev.AL_FBAA_MC_dacamp_prod_mc_final_{fw}` where campaign_code != "{campaign_code}";    
    {comment_out}INSERT INTO `wx-bq-poc.personal.AL_FBAA_MC_mc_final_tmp_2`
    
    select 
      key,
      banner,
      campaign_code,
      campaign_start_date,
      campaign_end_date,
      channel,
      medium,
      event,
      -1 as event_volume,
      -1 as event_reach,
      -1 as event_converted_crn,
      -1 as medium_reach,
      -1 as medium_converted_crn,
      channel_prob_norm
    from `wx-bq-poc.personal.AL_FBAA_MC_mc_final_tmp` where channel_prob_norm > 0;

    -- patch up dropped channels
    -- generate an entry in MC attribution for all CRN events not in it
    create or replace table {dacamp_prod_mc_final} as (
        with MC_old as (
        select distinct event
        -- from backup
        from `wx-bq-poc.wx_lty_digital_attribution_dev.AL_FBAA_MC_dacamp_prod_mc_final_{fw}`
        where campaign_code = "{campaign_code}"
        )
        , MC_new as (
        select distinct event
        -- this is the new MC attribution
        from `wx-bq-poc.personal.AL_FBAA_MC_mc_final_tmp_2`
        where campaign_code = "{campaign_code}"
        )
        , in_old_not_in_new as (
        select
            MC_old.event
        from MC_old
        where event is not null
        except distinct
        select
            MC_new.event
        from MC_new
            where event is not null
        )
        , new_rows as (
        select *
        -- grab old rows not in new
        from `wx-bq-poc.wx_lty_digital_attribution_dev.AL_FBAA_MC_dacamp_prod_mc_final_{fw}`
        where campaign_code = "{campaign_code}" and event in (select event from in_old_not_in_new)
        order by event
        )
        select *
        from `wx-bq-poc.personal.AL_FBAA_MC_mc_final_tmp_2`
        # where campaign_code = "{campaign_code}"
        union all
        select *
        from new_rows
    );

    -- show header to check if it worked
    select key, event, channel_prob_norm from {dacamp_prod_mc_final} where campaign_code = '{campaign_code}' order by key, event, channel_prob_norm;
    """.format(campaign_code=campaign_code, fw=fw, dacamp_prod_mc_final=dacamp_prod_mc_final, comment_out=comment_out)
    df_bq = (
        bqclient.query(query_string)
        .result()
        .to_dataframe(bqstorage_client=bqstorageclient)
    )
    logging.info("post MTA output uploaded to BQ")
    return df_bq

def run_post_MTA_processing(dacamp_prod_event_mw,
                            dacamp_prod_activation,
                            dacamp_prod_online_sales,
                            marketable_crn,
                            dacamp_prod_mc_final,
                            dacamp_prod_mc_final_crn,
                            filter_campaign=None,
                            bqclient=None,
                            bqstorageclient=None,
                            division="supers"):
    """
    Overview
        Runs Shanglin's post MTA processing (create CRN level attribution, further adjustments)
        Allocates non FB touchpoints at the CRN level, applying business logic
        Based on:
        https://bitbucket.org/wx_rds/safari-prod/src/master/markov_chain/bq_query/mc/mc_camp_mcfinal_crn_base.sql
    Arguments
        dacamp_prod_event_mw - copy of Shanglin's events, a BQ table
        dacamp_prod_activation - copy of Shanglin's activations, a BQ table
        dacamp_prod_online_sales - copy of Shanglin's online activations, a BQ table
        marketable_crn - copy of marketable CRNs, a BQ table
        dacamp_prod_mc_final - post MTA output (e.g. this is the MC input to the post MTA process), a BQ table
        dacamp_prod_mc_final_crn - BQ table to save the CRN level (output of this process)
        filter_campaign - dacamp_prod_mc_final_crn filtered for that campaign only
    Returns
        An empty DF if successful
    """
    if filter_campaign:
        filter_campaign_comment = ""
        campaign_code = filter_campaign
    else:
        filter_campaign_comment = "--"
        campaign_code=""
        
    if division == "supers":
        banner = "supermarkets"
    elif division in ("bigw", "big_w"):
        banner = "bigw"
    else:
        logging.error("division not in ('supers', 'bigw', 'big_w')")
        
    query_string = """
    ------------------------------------------------------
    -- REPLICATE SHANGLINS POST MTA CODE
    ------------------------------------------------------
    drop table if exists {dacamp_prod_mc_final_crn};
    create table {dacamp_prod_mc_final_crn} as
    (
        with onl_start_date as 
        (    
            select max(campaign_start_date) from {dacamp_prod_event_mw} where campaign_code = 'ONLINE'
        ),

        onl_end_date as 
        (
            select max(campaign_end_date) from {dacamp_prod_event_mw} where campaign_code = 'ONLINE'
        )

        SELECT distinct DATE_TRUNC(DATE_ADD((select * from onl_end_date),interval 0 day), week(Monday)) as week
            , c.crn
            , e.segment_cvm
            , e.segment_lifestage
            , f.segment_marketable
            , c.banner
            , c.campaign_code
            , c.campaign_type
            , c.campaign_start_date
            , c.campaign_end_date
            , c.tot_spend as total_sales
            , c.spend as inc_sales
            , channel_event
            , channel_prob_norm
            , case when a.channel is not null then a.channel else d.channel end as channel
            , case when a.medium is not null then a.medium else d.medium end as medium
            , case when a.event is not null then a.event else d.event end as event
            , case when channel_prob_norm is not null then channel_prob_norm / (sum(channel_prob_norm) OVER (PARTITION BY c.crn, b.key)) 
                    when channel_prob_norm is null and channel_event is not null then 0 
                    else 1 end as attributed_conversion
            , case when channel_prob_norm is not null then c.tot_spend * channel_prob_norm / (sum(channel_prob_norm) OVER (PARTITION BY c.crn, b.key)) 
                    when  channel_prob_norm is null and channel_event is not null then 0 
                    else c.tot_spend end as attributed_total_sales
            , case when channel_prob_norm is not null then c.spend * channel_prob_norm / (sum(channel_prob_norm) OVER (PARTITION BY c.crn, b.key)) 
                    WHEN channel_prob_norm is null and channel_event is not null then 0 
                     else c.spend end as attributed_inc_sales
        FROM
        (
            SELECT 
                concat('Total', '_', banner, '_', campaign_code, '_', cast(campaign_start_date as string), '_', cast(campaign_end_date as string)) as key
                , banner
                , crn
                , campaign_code
                , campaign_start_date
                , campaign_end_date
                , campaign_end_date_real
                , campaign_type
                , case when sum(tot_spend) is null then 0 else sum(tot_spend) end as tot_spend
                , case when sum(spend) is null then 0 else sum(spend) end as spend
            FROM
            (
                SELECT crn
                    , banner
                    , campaign_code
                    , campaign_start_date
                    , campaign_end_date
                    , campaign_end_date_real
                    , campaign_type
                    , min(time_utc) as conv_time
                    , sum(tot_spend) as tot_spend
                    , sum(spend) as spend
                FROM {dacamp_prod_activation}
                group by 1,2,3,4,5,6,7

                union all

                SELECT crn
                    , '{banner}' as banner
                    , 'ONLINE' as campaign_code
                    , (select * from onl_start_date) as campaign_start_date
                    , (select * from onl_end_date) as campaign_end_date
                    , (select * from onl_end_date) as campaign_end_date_real
                    , 'ONLINE' as campaign_type
                    , min(time_utc) as conv_time
                    , sum(tot_spend) as tot_spend
                    , sum(spend) as tot_spend
                FROM {dacamp_prod_online_sales}
                group by 1
            )
            {filter_campaign_comment} where campaign_code = "{campaign_code}"
            group by 1,2,3,4,5,6,7,8
        ) c 

        left join
        (        
            SELECT 
                concat('Total', '_', banner, '_', campaign_code, '_', cast(campaign_start_date as string), '_', cast(campaign_end_date as string)) as key
                , crn 
                , channel_event 
                , count(*) as event_volume
            FROM {dacamp_prod_event_mw} 
            group by 1,2,3
            order by 1,2,3
        ) b on b.key = c.key and b.crn = c.crn

        left join 
        (
            select *
        from {dacamp_prod_mc_final}
        ) a on a.key = b.key and a.event = b.channel_event 

        left join
        (
            SELECT a1.key
            , CASE WHEN channel is null and campaign_code not like '%NM%' then concat(SUBSTR(campaign_code,1,9), '_email') 
                    WHEN channel is null and campaign_code like '%NM%' then concat(SUBSTR(campaign_code,1,9), '_rw_app') 
                    else channel end as channel
            , CASE WHEN medium is null and campaign_code not like '%NM%' then concat(SUBSTR(campaign_code,1,9), '_email') 
                    WHEN medium is null and campaign_code like '%NM%' then concat(SUBSTR(campaign_code,1,9), '_rw_app') 
                    else medium end as medium
            ,  CASE WHEN event is null and campaign_code not like '%NM%' then concat(SUBSTR(campaign_code,1,9), '_email_open') 
                    WHEN event is null and campaign_code like '%NM%' then concat(SUBSTR(campaign_code,1,9), '_rw_app_imp') 
                    else event end as event
            , event_volume
            , event_reach
            , event_converted_crn
            , medium_reach
            , medium_converted_crn
            FROM
            (
                select distinct key, campaign_code
          from {dacamp_prod_mc_final}
            ) a1
            left join
            (    SELECT key
                    , channel
                    , medium
                    , event
                    , event_volume
                    , event_reach
                    , event_converted_crn
                    , medium_reach
                    , medium_converted_crn
          from {dacamp_prod_mc_final}
                where (campaign_code = 'ONLINE' and event like '%wow_web_view%')
                    or (campaign_code like '%NM%' and regexp_contains(event, r'(?i)rw_app_imp') and substr(event,1,9) = substr(campaign_code,1,9))
            or (campaign_code not like '%NM%' and campaign_code <> 'ONLINE' and regexp_contains(event, r'(?i)email_open') and substr(event,1,9) = substr(campaign_code,1,9))
            ) a2 on a1.key = a2.key
        ) d on c.key = d.key

        left join
        (
            SELECT crn, max(macro_segment_curr) as segment_cvm, max(lifestage) as segment_lifestage
            FROM `gcp-wow-rwds-ai-safari-prod.wdp_tables.redx_loyalty_customer_value_model`
            where Date(pw_end_date) = (select max(Date(pw_end_date)) from `gcp-wow-rwds-ai-safari-prod.wdp_tables.redx_loyalty_customer_value_model`)
            group by 1
        ) e on c.crn = e.crn

        left join
        (
            SELECT crn, max(marketable) as segment_marketable
            FROM {marketable_crn}
            group by 1
        ) f on c.crn = f.crn

    );
    """.format(dacamp_prod_event_mw=dacamp_prod_event_mw,
               dacamp_prod_activation=dacamp_prod_activation,
               dacamp_prod_online_sales=dacamp_prod_online_sales,
               marketable_crn=marketable_crn,
               dacamp_prod_mc_final=dacamp_prod_mc_final,
               dacamp_prod_mc_final_crn=dacamp_prod_mc_final_crn,
               filter_campaign_comment=filter_campaign_comment,
               campaign_code=campaign_code,
               banner=banner)
    df_bq = (
        bqclient.query(query_string)
        .result()
        .to_dataframe(bqstorage_client=bqstorageclient)
    )
    return df_bq

def get_post_MTA_inc_sales(campaign_code, data_date, groupby="channel", fpath_out=None):
    """
    Overview
        Grabs the final Safari post-MTA results (including manual FBAA run) from GCP
    Arguments
        campaign_code
        data_date - date of MTA run, check gs://digital-attribution-data/phase2/markov_out/
        groupby="channel"
        fpath_out=None
    Returns
        Dataframe of grouped post-MTA results
    """
    df = pd.read_csv('gs://digital-attribution-data/phase2/markov_out/' + data_date + '/mc_final_agg.csv')
    mask = (df.campaign_code == campaign_code) & (df.segment == "Total")
    df_group = df[mask].groupby(groupby).sum()
    df_group['attributed_inc_sales_percent'] = (df_group['attributed_inc_sales'] / df_group['attributed_inc_sales'].sum()) * 100
    if fpath_out:
        df_group.to_csv(fpath_out + "post_MTA_results_" + data_date + ".csv")
    return df_group

def compare_MTA_results(dacamp_prod_mc_final_crn_new,
                        dacamp_prod_mc_final_crn_old,
                        campaign_code,
                        safari_run_date,
                        fpath_out,
                        fw,
                        send_email=False,
                        compare_to_gcp_results=False,
                        bqclient=None,
                        bqstorageclient=None):
    """
    Overview
        Compares CRN level post MTA results of the old and new method.
    Arguments
        dacamp_prod_mc_final_crn_new
        dacamp_prod_mc_final_crn_old
        campaign_code
        safari_run_date
    Returns
        Dataframe of comparison results (old vs. new method)
    """
    query_string = """
    -- new MTA results vs old
    with results_AL as (  -- new results
    SELECT
      campaign_code,
      campaign_start_date,
      channel,
      medium,
      event,
      coalesce(channel_event, "DEFAULT") as channel_event,
      AVG(channel_prob_norm) as channel_prob_norm,
      sum(attributed_inc_sales) as attributed_inc_sales,
      count(*) as count
    FROM {dacamp_prod_mc_final_crn_new}
    where campaign_code = "{campaign_code}"
    and channel is not null
    and attributed_inc_sales != 0
    group by 1,2,3,4,5,6
    order by 1,2,3,4,5,6
    ),
    results_Shanglin as (
    SELECT
      campaign_code,
      campaign_start_date,
      channel,
      medium,
      event,
      coalesce(channel_event, "DEFAULT") as channel_event,
      AVG(channel_prob_norm) as channel_prob_norm,
      sum(attributed_inc_sales) as attributed_inc_sales,
      count(*) as count
    FROM {dacamp_prod_mc_final_crn_old}
    where campaign_code = "{campaign_code}"
    and channel is not null
    and attributed_inc_sales != 0
    group by 1,2,3,4,5,6
    order by 1,2,3,4,5,6
    )
    select
      coalesce(a.campaign_code, b.campaign_code) as campaign_code,
      coalesce(a.campaign_start_date, b.campaign_start_date) as campaign_start_date,
      coalesce(a.channel, b.channel) as channel,
      coalesce(a.medium, b.medium) as medium,
      coalesce(a.event, b.event) as event,
      coalesce(a.channel_event, b.channel_event) as channel_event,
      a.channel_prob_norm as channel_prob_norm_AL,
      b.channel_prob_norm as channel_prob_norm_shanglin,
      a.attributed_inc_sales as attributed_inc_sales_AL,
      b.attributed_inc_sales as attributed_inc_sales_shanglin,
      a.count as count_AL,
      b.count as count_shanglin
    from results_AL as a
    full outer join results_Shanglin as b
      on a.event = b.event and a.channel_event = b.channel_event and a.campaign_start_date = b.campaign_start_date
    order by coalesce(a.campaign_start_date, b.campaign_start_date), coalesce(a.event, b.event), coalesce(a.channel_event, b.channel_event)
    ;
    """.format(dacamp_prod_mc_final_crn_new=dacamp_prod_mc_final_crn_new, dacamp_prod_mc_final_crn_old=dacamp_prod_mc_final_crn_old,
               campaign_code=campaign_code)

    results = (
        bqclient.query(query_string)
        .result()
        .to_dataframe(bqstorage_client=bqstorageclient)
    )
    
    if not os.path.exists(campaign_code + "/comparison"):
        logging.info("directory " + campaign_code + "/comparison does not exist, creating directory")
        os.makedirs(campaign_code + "/comparison")
    
    results.loc[:, "channel"] = results.channel.str.strip("_")
    results.loc[:, "medium"] = results.medium.str.strip("_")
    results.loc[:, "event"] = results.event.str.strip("_")
    
    plt.title("MTA results current vs new " + campaign_code + ": " + fw)
    sns.regplot(x="attributed_inc_sales_AL", y="attributed_inc_sales_shanglin", data=results)
    plt.savefig(fpath_out + "_comp_scatter_" + campaign_code + "_" + fw +  ".png")
    plt.show()    
    
    plt.title("MTA results current vs new (excluding default channel) " + campaign_code + ": " + fw)
    sns.regplot(x="attributed_inc_sales_AL", y="attributed_inc_sales_shanglin", data=results[results.channel_event != "DEFAULT"])    
    plt.savefig(fpath_out + "_comp_scatter_ex_def_channel_" + campaign_code + "_" + fw +  ".png")
    plt.show()
    
    if compare_to_gcp_results:
        # pull in final results from Safari, saved to GCP 
        results_comparison = pd.merge(left=results.groupby("event").sum(), right=get_post_MTA_inc_sales(campaign_code, safari_run_date, groupby="event"),
                                      how="outer", on="event").loc[:, ['channel_prob_norm_AL', 'channel_prob_norm_shanglin', 'attributed_inc_sales_AL', 'attributed_inc_sales_shanglin', 'attributed_inc_sales']]
        results_comparison = results_comparison.rename({"attributed_inc_sales":"attributed_inc_sales_safari"}, axis="columns")   
    else:
        results_comparison = results.groupby("event").sum().loc[:, ['channel_prob_norm_AL', 'channel_prob_norm_shanglin', 'attributed_inc_sales_AL', 'attributed_inc_sales_shanglin']]   
    
#     plt.title("MTA results current Safari vs new (grouped) " + campaign_code + ": " + fw)
#     sns.regplot(x="attributed_inc_sales_AL", y="attributed_inc_sales_safari", data=results_comparison)
#     plt.savefig(fpath_out + "_comp_scatter_grouped_" + campaign_code + "_" + fw +  ".png")
#     plt.show()    
    
    plt.subplots(figsize=(30, 15))
    plt.title("MTA results current vs new " + campaign_code + ": " + fw)
    if compare_to_gcp_results:
        sns.heatmap(results_comparison.loc[:, ['attributed_inc_sales_AL', 'attributed_inc_sales_shanglin', 'attributed_inc_sales_safari']], cmap='cividis', annot=True, fmt=".2f")
    else:
        sns.heatmap(results_comparison.loc[:, ['attributed_inc_sales_AL', 'attributed_inc_sales_shanglin']], cmap='cividis', annot=True, fmt=".2f")

    plt.savefig(fpath_out + "_comp_heatmap_" + campaign_code + "_" + fw +  ".png")
    plt.show()    
    
    results_comparison.loc["Total",:] = results_comparison.sum(numeric_only=True)
    
    if send_email:
        message_body = """
        Results saved here:
        {output_table}
        Comparison of old and new MTA FB method
        {results}
        """.format(output_table=dacamp_prod_mc_final_crn_new, 
                   results=str(results_comparison.loc[:, ['attributed_inc_sales_AL', 'attributed_inc_sales_shanglin']]))
        
        email_notifications.send_email_notification(email_recipient=EMAIL_RECIPIENT,
                                                    subject='wow.bot🤖: 🔎✔️ RESULTS FOR MTA with FB run for fw ' +  fw + ": " + campaign_code,
                                                    message_body=message_body,
                                                    fpath_image=fpath_out + "_comp_heatmap_" + campaign_code + "_" + fw +  ".png")
    return results, results_comparison

def allocate_fb_inc_sales(campaign_code,
                          campaign_start_date,
                          campaign_end_date,
                          fpath_results,
                          dacamp_prod_mc_final_crn_w_fb,
                          dacamp_prod_mc_final_crn,
                          increase_inc_sales=False,
                          bqclient=None,
                          bqstorageclient=None):
    """
    Overview
        Increase the size of the pie (discovered via Xueyuan's FBAA bit).
    Examples
    >>> allocate_fb_inc_sales(campaign_code='OSP-2373', campaign_start_date='2021-07-05', campaign_end_date='2021-07-11', fpath_results="OSP-2373/res_w_fb_weighted_OSP-2373_2021-07-05_2021-07-11_7.csv", dacamp_prod_mc_final_crn_w_fb="`wx-bq-poc.personal.AL_FBAA_MC_dacamp_prod_mc_final_crn_w_fb_2021-07-05_OSP-2373`", dacamp_prod_mc_final_crn="`wx-bq-poc.personal.AL_FBAA_MC_dacamp_prod_mc_final_crn_2021-07-05_OSP-2373`", bqclient=bqclient, bqstorageclient=bqstorageclient, increase_inc_sales=False)
    FB (inc IG):total attribution ratio is 0.027743
    non-FB:total attribution ratio is 0.972257
    IG:FB attribution ratio is 0.220991
    NOT INCREASING inc sales... selected FB (inc IG) scaling ratio is (for missing slice of pie) 0.027743
    fb inc sales for OSP-2373 increased by -0.0
    touchpoint count increased by 1171244
    inc_sales_before  inc_sales_after  inc_sales_increase  count_before  count_after  count_increase  inc_sales_FB_before  inc_sales_FB_after  inc_sales_IG_before  inc_sales_IG_after
    0        11417871.0       11417871.0                -0.0        864800      2036044         1171244                  NaN            246760.0                  NaN             70001.0
   
    >>> allocate_fb_inc_sales(campaign_code='CVM-1661', campaign_start_date='2021-03-01', campaign_end_date='2021-03-07', fpath_results="CVM-1661/res_weighted_CVM-1661_2021-03-01_2021-03-07_7.csv", dacamp_prod_mc_final_crn_w_fb="`wx-bq-poc.personal.AL_FBAA_MC_dacamp_prod_mc_final_crn_w_fb_2021-03-01_CVM-1661`", dacamp_prod_mc_final_crn="`wx-bq-poc.personal.AL_FBAA_MC_dacamp_prod_mc_final_crn_2021-03-01_CVM-1661`", bqclient=bqclient, bqstorageclient=bqstorageclient, increase_inc_sales=True)
    FB (inc IG):total attribution ratio is 0.319642
    non-FB:total attribution ratio is 0.680358
    IG:FB attribution ratio is 0.0
    INCREASING inc sales... selected FB (inc IG) scaling ratio is (for missing slice of pie) 0.469814
    fb inc sales for CVM-1661 increased by 0.469814
    touchpoint count increased by 1360429
    inc_sales_before  inc_sales_after  inc_sales_increase  count_before  count_after  count_increase  inc_sales_FB_before  inc_sales_FB_after  inc_sales_IG_before  inc_sales_IG_after
    0         1553107.0        2282778.0            729671.0       1389982      2750411         1360429                  NaN            729671.0                  NaN                 NaN
    >>> allocate_fb_inc_sales(campaign_code='CVM-1661', campaign_start_date='2021-02-15', campaign_end_date='2021-02-21', fpath_results="CVM-1661/res_weighted_CVM-1661_2021-02-15_2021-02-21_7.csv", dacamp_prod_mc_final_crn_w_fb="`wx-bq-poc.personal.AL_FBAA_MC_dacamp_prod_mc_final_crn_w_fb_2021-02-15_CVM-1661`", dacamp_prod_mc_final_crn="`wx-bq-poc.personal.AL_FBAA_MC_dacamp_prod_mc_final_crn_2021-02-15_CVM-1661`", bqclient=bqclient, bqstorageclient=bqstorageclient, increase_inc_sales=True)
    FB (inc IG):total attribution ratio is 0.472837
    non-FB:total attribution ratio is 0.527163
    IG:FB attribution ratio is 0.0
    INCREASING inc sales... selected FB (inc IG) scaling ratio is (for missing slice of pie) 0.896947
    fb inc sales for CVM-1661 increased by 0.896947
    touchpoint count increased by 1799173
    inc_sales_before  inc_sales_after  inc_sales_increase  count_before  count_after  count_increase  inc_sales_FB_before  inc_sales_FB_after  inc_sales_IG_before  inc_sales_IG_after
    0         1180894.0        2240093.0           1059199.0       1835804      3634977         1799173                  NaN           1059199.0                  NaN                 NaN
    
    >>> allocate_fb_inc_sales(campaign_code='OSP-2373', campaign_start_date='2021-06-28', campaign_end_date='2021-07-04', fpath_results="OSP-2373/res_w_fb_weighted_OSP-2373_2021-06-28_2021-07-04_7.csv", dacamp_prod_mc_final_crn_w_fb="`wx-bq-poc.personal.AL_FBAA_MC_dacamp_prod_mc_final_crn_w_fb_2021-06-28_OSP-2373`", dacamp_prod_mc_final_crn="`wx-bq-poc.personal.AL_FBAA_MC_dacamp_prod_mc_final_crn_2021-06-28_OSP-2373`", bqclient=bqclient, bqstorageclient=bqstorageclient, increase_inc_sales=False)
    FB (inc IG):total attribution ratio is 0.028613
    non-FB:total attribution ratio is 0.971387
    IG:FB attribution ratio is 0.0
    NOT INCREASING inc sales... selected FB (inc IG) scaling ratio is (for missing slice of pie) 0.028613
    fb inc sales for OSP-2373 increased by -0.0
    touchpoint count increased by 608613
    inc_sales_before  inc_sales_after  inc_sales_increase  count_before  count_after  count_increase  inc_sales_FB_before  inc_sales_FB_after  inc_sales_IG_before  inc_sales_IG_after
    0        13944972.0       13944972.0                -0.0        855650      1464263          608613                  NaN            399004.0                  NaN                 NaN
   
    >>> allocate_fb_inc_sales(campaign_code='OSP-2373', campaign_start_date='2021-06-21', campaign_end_date='2021-06-27', fpath_results="OSP-2373/res_w_fb_weighted_OSP-2373_2021-06-21_2021-06-27_7.csv", dacamp_prod_mc_final_crn_w_fb="`wx-bq-poc.personal.AL_FBAA_MC_dacamp_prod_mc_final_crn_w_fb_2021-06-21_OSP-2373`", dacamp_prod_mc_final_crn="`wx-bq-poc.personal.AL_FBAA_MC_dacamp_prod_mc_final_crn_2021-06-21_OSP-2373`", bqclient=bqclient, bqstorageclient=bqstorageclient, increase_inc_sales=False)
    FB (inc IG):total attribution ratio is 0.030516
    non-FB:total attribution ratio is 0.969484
    IG:FB attribution ratio is 0.0
    NOT INCREASING inc sales... selected FB (inc IG) scaling ratio is (for missing slice of pie) 0.030516
    fb inc sales for OSP-2373 increased by -0.0
    touchpoint count increased by 675093
    inc_sales_before  inc_sales_after  inc_sales_increase  count_before  count_after  count_increase  inc_sales_FB_before  inc_sales_FB_after  inc_sales_IG_before  inc_sales_IG_after
    0        11851461.0       11851461.0                -0.0        880183      1555276          675093                  NaN            361654.0                  NaN                 NaN

    """
    # extract scaling factor from package results
    df = pd.read_csv(fpath_results)    
    mask_fb = df.channel_name.str.startswith('FB_')
    mask_ig = df.channel_name.str.contains('Instagram')
    
    # if there are no FB/IG inc sales, skip adding new rows
    if sum(mask_fb) == 0:
        logging.warning("no FB attribution, not adding new rows")
        query_string = """
          ---------------------------------------------------------------------------
          -- ADDING IN FB INC SALES
          ---------------------------------------------------------------------------
        CREATE OR REPLACE TABLE
          {dacamp_prod_mc_final_crn_w_fb} AS
        SELECT
            week
            , crn
            , segment_cvm
            , segment_lifestage
            , segment_marketable
            , banner
            , campaign_code
            , campaign_type
            , campaign_start_date
            , campaign_end_date
            , total_sales
            , inc_sales
            , channel_event
            , channel_prob_norm
            , channel
            , medium
            , event
            , attributed_conversion
            , attributed_total_sales
            , attributed_inc_sales
        FROM
          {dacamp_prod_mc_final_crn}
          ;
          -- CHECKS
        SELECT
          -- (select SUM(attributed_inc_sales) FROM {dacamp_prod_mc_final_crn} where campaign_code = '{campaign_code}') as inc_sales_before_ws_only,
          round((select SUM(attributed_inc_sales) FROM {dacamp_prod_mc_final_crn})) as inc_sales_before,
          round((select SUM(attributed_inc_sales) FROM {dacamp_prod_mc_final_crn_w_fb})) as inc_sales_after,
          round((select SUM(attributed_inc_sales) FROM {dacamp_prod_mc_final_crn_w_fb}) - (select SUM(attributed_inc_sales) FROM {dacamp_prod_mc_final_crn})) as inc_sales_increase,
          (select count(*) FROM {dacamp_prod_mc_final_crn}) as count_before,
          (select count(*) FROM {dacamp_prod_mc_final_crn_w_fb}) as count_after,
          (select count(*) FROM {dacamp_prod_mc_final_crn_w_fb}) - (select count(*) FROM {dacamp_prod_mc_final_crn}) as count_increase,
          round((select SUM(attributed_inc_sales) FROM {dacamp_prod_mc_final_crn} where channel = '{campaign_code}_FB')) as inc_sales_FB_before,
          round((select SUM(attributed_inc_sales) FROM {dacamp_prod_mc_final_crn_w_fb} where channel = '{campaign_code}_FB')) as inc_sales_FB_after,
          round((select SUM(attributed_inc_sales) FROM {dacamp_prod_mc_final_crn} where channel = '{campaign_code}_IG')) as inc_sales_IG_before,
          round((select SUM(attributed_inc_sales) FROM {dacamp_prod_mc_final_crn_w_fb} where channel = '{campaign_code}_IG')) as inc_sales_IG_after
        ;
        """.format(dacamp_prod_mc_final_crn_w_fb=dacamp_prod_mc_final_crn_w_fb,
                   dacamp_prod_mc_final_crn=dacamp_prod_mc_final_crn,
                   campaign_code=campaign_code,
                   campaign_start_date=campaign_start_date,
                   campaign_end_date=campaign_end_date)
        results = (
            bqclient.query(query_string)
            .result()
            .to_dataframe(bqstorage_client=bqstorageclient)
        )
        
        # exit after skipping adding of FB rows
        return results
        
    attr_fb = df.loc[mask_fb].normalised_conversion_value.sum()
    
    attr_non_fb = df.loc[~mask_fb].normalised_conversion_value.sum()
    
    fb_scaling = attr_fb / attr_non_fb
    
    attr_fb_ratio = attr_fb / (attr_fb + attr_non_fb)
    attr_non_fb_ratio = attr_non_fb / (attr_fb + attr_non_fb)
    
    ig_ratio = df[mask_ig].normalised_conversion_value.sum() / attr_fb
    
    logging.info("FB (inc IG):total attribution ratio is {0}".format(round(attr_fb_ratio, 6)))
    logging.info("non-FB:total attribution ratio is {0}".format(round(attr_non_fb_ratio, 6)))
    logging.info("IG:FB attribution ratio is {0}".format(round(ig_ratio, 6)))
    
    if increase_inc_sales:
        increase_inc_sales_comment = "--"
        attr_fb_selected = fb_scaling
        logging.info("INCREASING inc sales... selected FB (inc IG) scaling ratio is (for missing slice of pie) {0}".format(round(attr_fb_selected, 6)))
    else:  # reslice pie, maintain total inc sales
        increase_inc_sales_comment = ""
        attr_fb_selected = attr_fb_ratio            
        logging.info("NOT INCREASING inc sales... selected FB (inc IG) scaling ratio is (for missing slice of pie) {0}".format(round(attr_fb_selected, 6)))
    
    # toggle add new rows or not
    if ig_ratio <= 0.0000001:
        ig_off_toggle = "--"
    else:
        ig_off_toggle = ""
    
    query_string = """
      ---------------------------------------------------------------------------
      -- ADDING IN FB INC SALES
      ---------------------------------------------------------------------------
    CREATE OR REPLACE TABLE
      {dacamp_prod_mc_final_crn_w_fb} AS
    WITH
      unique_crn AS (  -- base to create more entries, one row per CRN and targeted campaign period
          SELECT
            DISTINCT week,
            crn,
            segment_cvm,
            segment_lifestage,
            segment_marketable,
            banner,
            campaign_code,
            campaign_type,
            -- new bits for pro rata allocation
            campaign_start_date,
            campaign_end_date,
            -- start of exposure period (cupped by start of fw)
            GREATEST("{campaign_start_date}", campaign_start_date) AS campaign_start_exposure,
            -- end of exposure period (capped by start of fw)
            LEAST("{campaign_end_date}", campaign_end_date) AS campaign_end_exposure,
            -- exposure, capped and cupped by 7 and 1 as a safeguard for incorrect values
            LEAST(7, GREATEST(1, (DATE_DIFF(LEAST("{campaign_end_date}", campaign_end_date), GREATEST("{campaign_start_date}", campaign_start_date), DAY) + 1)))
                AS exposure
          FROM
            {dacamp_prod_mc_final_crn}
          WHERE
            campaign_code = "{campaign_code}"),
      total_inc_sales AS (
          SELECT
            SUM(attributed_inc_sales)
          FROM
            {dacamp_prod_mc_final_crn}
          WHERE
            campaign_code = "{campaign_code}" ),
      total_crn_count AS (
          SELECT
            COUNT(*)
          FROM
            unique_crn ), 
      total_exposure AS (
          SELECT
            SUM(exposure)
          FROM
            unique_crn ), 
      new_rows AS (
          SELECT
            unique_crn.week,
            unique_crn.crn,
            unique_crn.segment_cvm,
            unique_crn.segment_lifestage,
            unique_crn.segment_marketable,
            unique_crn.banner,
            unique_crn.campaign_code,
            unique_crn.campaign_type,
            --PARSE_DATE('%Y-%m-%d',
            --  '{campaign_start_date}') AS campaign_start_date,
            --PARSE_DATE('%Y-%m-%d',
            --  '{campaign_end_date}') AS campaign_end_date,
            unique_crn.campaign_start_date,
            unique_crn.campaign_end_date,            
            
            0 AS total_sales,
            0 AS inc_sales,
            "" AS channel_event,
            -1 AS channel_prob_norm,
            '{campaign_code}_FB' AS channel,
            '{campaign_code}_FB' AS medium,
            '{campaign_code}_FB_impsclicks' AS event,
            0 AS attributed_conversion,
            0 AS attributed_total_sales,
            --{attr_fb_selected} * (
            --    SELECT
            --      *
            --    FROM
            --      total_inc_sales) / (
            --    SELECT
            --      *
            --    FROM
            --      total_crn_count) AS attributed_inc_sales  -- uniformly allocate fb inc sales to each CRN
            (1 - {ig_ratio}) * {attr_fb_selected} * unique_crn.exposure * (SELECT * FROM total_inc_sales) / (SELECT * FROM total_exposure)
                AS attributed_inc_sales  -- uniformly allocate fb inc sales to each CRN
          FROM
            unique_crn),
      new_rows_IG AS (
          SELECT
            unique_crn.week,
            unique_crn.crn,
            unique_crn.segment_cvm,
            unique_crn.segment_lifestage,
            unique_crn.segment_marketable,
            unique_crn.banner,
            unique_crn.campaign_code,
            unique_crn.campaign_type,
            --PARSE_DATE('%Y-%m-%d',
            --  '{campaign_start_date}') AS campaign_start_date,
            --PARSE_DATE('%Y-%m-%d',
            --  '{campaign_end_date}') AS campaign_end_date,
            unique_crn.campaign_start_date,
            unique_crn.campaign_end_date,            
            
            0 AS total_sales,
            0 AS inc_sales,
            "" AS channel_event,
            -1 AS channel_prob_norm,
            '{campaign_code}_IG' AS channel,
            '{campaign_code}_IG' AS medium,
            '{campaign_code}_IG_impsclicks' AS event,
            0 AS attributed_conversion,
            0 AS attributed_total_sales,
            --{attr_fb_selected} * (
            --    SELECT
            --      *
            --    FROM
            --      total_inc_sales) / (
            --    SELECT
            --      *
            --    FROM
            --      total_crn_count) AS attributed_inc_sales  -- uniformly allocate fb inc sales to each CRN
            {ig_ratio} * {attr_fb_selected} * unique_crn.exposure * (SELECT * FROM total_inc_sales) / (SELECT * FROM total_exposure)
                AS attributed_inc_sales  -- uniformly allocate fb inc sales to each CRN
          FROM
            unique_crn)   
    SELECT
        week
        , crn
        , segment_cvm
        , segment_lifestage
        , segment_marketable
        , banner
        , campaign_code
        , campaign_type
        , campaign_start_date
        , campaign_end_date
        , total_sales
        , inc_sales
        , channel_event
        , channel_prob_norm
        , channel
        , medium
        , event
        , attributed_conversion
        , attributed_total_sales
        , attributed_inc_sales {increase_inc_sales_comment} * {attr_non_fb_ratio} as attributed_inc_sales
    FROM
      {dacamp_prod_mc_final_crn}
      
    UNION ALL
    
    SELECT * FROM new_rows
      
    {ig_off_toggle}UNION ALL
    
    {ig_off_toggle} SELECT * FROM new_rows_IG
      ;
      -- CHECKS
    SELECT
      -- (select SUM(attributed_inc_sales) FROM {dacamp_prod_mc_final_crn} where campaign_code = '{campaign_code}') as inc_sales_before_ws_only,
      round((select SUM(attributed_inc_sales) FROM {dacamp_prod_mc_final_crn})) as inc_sales_before,
      round((select SUM(attributed_inc_sales) FROM {dacamp_prod_mc_final_crn_w_fb})) as inc_sales_after,
      round((select SUM(attributed_inc_sales) FROM {dacamp_prod_mc_final_crn_w_fb}) - (select SUM(attributed_inc_sales) FROM {dacamp_prod_mc_final_crn})) as inc_sales_increase,
      (select count(*) FROM {dacamp_prod_mc_final_crn}) as count_before,
      (select count(*) FROM {dacamp_prod_mc_final_crn_w_fb}) as count_after,
      (select count(*) FROM {dacamp_prod_mc_final_crn_w_fb}) - (select count(*) FROM {dacamp_prod_mc_final_crn}) as count_increase,
      round((select SUM(attributed_inc_sales) FROM {dacamp_prod_mc_final_crn} where channel = '{campaign_code}_FB')) as inc_sales_FB_before,
      round((select SUM(attributed_inc_sales) FROM {dacamp_prod_mc_final_crn_w_fb} where channel = '{campaign_code}_FB')) as inc_sales_FB_after,
      round((select SUM(attributed_inc_sales) FROM {dacamp_prod_mc_final_crn} where channel = '{campaign_code}_IG')) as inc_sales_IG_before,
      round((select SUM(attributed_inc_sales) FROM {dacamp_prod_mc_final_crn_w_fb} where channel = '{campaign_code}_IG')) as inc_sales_IG_after
    ;
    """.format(dacamp_prod_mc_final_crn_w_fb=dacamp_prod_mc_final_crn_w_fb,
               dacamp_prod_mc_final_crn=dacamp_prod_mc_final_crn,
               campaign_code=campaign_code,
               fb_scaling=fb_scaling,
               campaign_start_date=campaign_start_date,
               campaign_end_date=campaign_end_date,
               attr_fb_selected=attr_fb_selected,
               attr_non_fb_ratio=attr_non_fb_ratio,
               increase_inc_sales_comment=increase_inc_sales_comment,
               ig_ratio=ig_ratio,
               ig_off_toggle=ig_off_toggle)
    results = (
        bqclient.query(query_string)
        .result()
        .to_dataframe(bqstorage_client=bqstorageclient)
    )

    # check increase went to plan
    fb_increase = results.inc_sales_increase.values[0] / results.inc_sales_before.values[0]
    logging.info("fb inc sales for {0} increased by {1}".format(campaign_code, round(fb_increase, 6)))
    logging.info("touchpoint count increased by {0}".format(results.count_increase.values[0]))
    
    if increase_inc_sales:
        assert abs(fb_increase - attr_fb_selected) < 0.00001
    else:        
        assert abs(fb_increase - 0) < 0.00001
    
    return results

def downstream_process(fpath_results,
                       campaign_code,
                       fw,
                       safari_run_date,
                       fpath_comparison_results="01_results/",
                       BQ_output_suffix="",
                       create_dacamp_prod_mc_final=False,
                       have_historical_marketable_crn=True,
                       which_mc_crn_output="backup",
                       filter_campaign=True,
                       allocate_fb=False,
                       increase_inc_sales=False,  # increase inc sales for FB allocation
                       bqclient=None,
                       bqstorageclient=None,
                       division="supers"):
    """
    Overview
        runs the entire post MC process and compares it to the current BAU runs
    Arguments
        fpath_results - filepath of the CSV with the saved MC results, using Markov Squad's new package
        campaign_code
        fw
        safari_run_date
    Returns
        Dataframe which compares the old and new MTA results
    """
    logging.info(">>> Step 1: convert MC results to post MTA_format and upload to BQ")
    convert_to_post_MTA_format(fpath_results = fpath_results,
                               campaign_code = campaign_code,
                               fw=fw,
                               # this is the table that passed to run_post_MTA_processing with new MC results
                               dacamp_prod_mc_final="`wx-bq-poc.personal.AL_FBAA_MC_dacamp_prod_mc_final_" + fw + "_" + BQ_output_suffix + "`",
                               create_dacamp_prod_mc_final=create_dacamp_prod_mc_final,
                               bqclient=bqclient,
                               bqstorageclient=bqstorageclient,
                               division=division)        
    logging.info("exported to `wx-bq-poc.personal.AL_FBAA_MC_dacamp_prod_mc_final_" + fw + "_" + BQ_output_suffix + "`")
    logging.info(">>> Step 2: run post MTA processing (Shanglin's process)")
    if have_historical_marketable_crn:
        marketable_crn = "`wx-bq-poc.wx_lty_digital_attribution_dev.AL_FBAA_MC_marketable_crn_" + fw + "`"
    else:
        marketable_crn = "`gcp-wow-rwds-ai-safari-prod.digital_attribution_modelling.marketable_crn`"

    if which_mc_crn_output == "backup":
        dacamp_prod_mc_final_crn_old = "`wx-bq-poc.wx_lty_digital_attribution_dev.AL_FBAA_MC_dacamp_prod_mc_final_crn_" + fw + "`"
    elif which_mc_crn_output == "Xueyuan":
        dacamp_prod_mc_final_crn_old = "`wx-bq-poc.xueyuan.dacamp_prod_mc_union_rerun_" + fw[0:4] + "_" + fw[5:7] + "_" + fw[-2:] + "`"
    elif which_mc_crn_output == "Weixing":
        dacamp_prod_mc_final_crn_old = "`wx-bq-poc.Weixing.dacamp_prod_mc_final_crn_mta_before_fbaa_backup_fw" + fw[0:4] + fw[5:7] + fw[-2:] + "`"
    if filter_campaign:
        filter_campaign_argument=campaign_code
    else:
        filter_campaign_argument=None
    run_post_MTA_processing(dacamp_prod_event_mw="`wx-bq-poc.wx_lty_digital_attribution_dev.AL_FBAA_MC_dacamp_prod_event_mw_" + fw + "`",
                            dacamp_prod_activation="`wx-bq-poc.wx_lty_digital_attribution_dev.AL_FBAA_MC_dacamp_prod_activation_" + fw + "`",
                            dacamp_prod_online_sales="`wx-bq-poc.wx_lty_digital_attribution_dev.AL_FBAA_MC_dacamp_prod_online_sales_" + fw + "`",
                            marketable_crn=marketable_crn,                            
                            # new MTA MC output   
                            dacamp_prod_mc_final="`wx-bq-poc.personal.AL_FBAA_MC_dacamp_prod_mc_final_" + fw + "_" + BQ_output_suffix + "`",
                            dacamp_prod_mc_final_crn="`wx-bq-poc.personal.AL_FBAA_MC_dacamp_prod_mc_final_crn_" + fw + "_" + BQ_output_suffix + "`",
                            filter_campaign=filter_campaign_argument,
                            bqclient=bqclient,
                            bqstorageclient=bqstorageclient,
                            division=division
                           )
    logging.info("exported to `wx-bq-poc.personal.AL_FBAA_MC_dacamp_prod_mc_final_crn_" + fw + "_" + BQ_output_suffix + "`")
    logging.info(">>> Step 3: Compare results to BAU run")
    results, results_comparison = compare_MTA_results(dacamp_prod_mc_final_crn_new="`wx-bq-poc.personal.AL_FBAA_MC_dacamp_prod_mc_final_crn_" + fw + "_" + BQ_output_suffix + "`",  # new CRN level post-MTA processing
                                                      # old CRN level post-MTA processing
                                                      dacamp_prod_mc_final_crn_old=dacamp_prod_mc_final_crn_old, 
                                                      campaign_code=campaign_code,
                                                      safari_run_date=safari_run_date,
                                                      fpath_out=fpath_comparison_results,
                                                      fw=fw,
                                                      bqclient=bqclient,
                                                      bqstorageclient=bqstorageclient)
    results.to_csv(fpath_comparison_results + "_comp_" + campaign_code + "_" + fw + ".csv")
    results_comparison.to_csv(fpath_comparison_results + "_comp_grouped_" + campaign_code + "_" + fw + ".csv")
    if allocate_fb:
        logging.info(">>> Step 4 (only for fws with FB): Distribute FB inc sales")        
        # end of fw
        datetime_object = datetime.datetime.strptime(fw, '%Y-%m-%d') + datetime.timedelta(days=6)
        end_of_fw = datetime_object.strftime('%Y-%m-%d')
        logging.debug(fw + " ... end of fw is " + end_of_fw)
        
        allocate_fb_inc_sales(campaign_code=campaign_code,
                              campaign_start_date=fw,
                              campaign_end_date=end_of_fw,
                              fpath_results=fpath_results,
                              dacamp_prod_mc_final_crn_w_fb="`wx-bq-poc.personal.AL_FBAA_MC_dacamp_prod_mc_final_crn_w_fb_" + fw + "_" + BQ_output_suffix + "`",
                              dacamp_prod_mc_final_crn="`wx-bq-poc.personal.AL_FBAA_MC_dacamp_prod_mc_final_crn_" + fw + "_" + BQ_output_suffix + "`",
                              bqclient=bqclient,
                              bqstorageclient=bqstorageclient,
                              increase_inc_sales=increase_inc_sales)
        logging.info("exported dacamp_prod_mc_final_crn_w_fb to `wx-bq-poc.personal.AL_FBAA_MC_dacamp_prod_mc_final_crn_w_fb_" + fw + "_" + BQ_output_suffix + "`")
        results, results_comparison = compare_MTA_results(dacamp_prod_mc_final_crn_new="`wx-bq-poc.personal.AL_FBAA_MC_dacamp_prod_mc_final_crn_w_fb_" + fw + "_" + BQ_output_suffix + "`",
                                                          dacamp_prod_mc_final_crn_old=dacamp_prod_mc_final_crn_old,
                                                          campaign_code=campaign_code,
                                                          safari_run_date=safari_run_date,
                                                          fpath_out=fpath_comparison_results + "_fb",
                                                          fw=fw,
                                                          send_email=True,
                                                          bqclient=bqclient,
                                                          bqstorageclient=bqstorageclient)
        results.to_csv(fpath_comparison_results + "_fb_comp_" + campaign_code + "_" + fw + ".csv")
        results_comparison.to_csv(fpath_comparison_results + "_fb_comp_grouped_" + campaign_code + "_" + fw + ".csv")
    return results_comparison, results

def run_downstream_process(campaign_code, division, fw=None, bqclient=None, bqstorageclient=None, ttl_flag=False):
    # if no fw is provided, use the latest week
    if fw is None:
        today = datetime.date.today()
        last_monday = today + datetime.timedelta(days=-today.weekday(), weeks=-1)
        fw = str(last_monday)
    
    # get END of fw
    datetime_object = datetime.datetime.strptime(fw, '%Y-%m-%d')
    datetime_object_plus_6_days = datetime_object + datetime.timedelta(days=6)
    fw_end = datetime_object_plus_6_days.strftime("%Y-%m-%d")
    logging.info("Running downstream process for " + campaign_code + " fw:" + fw + " to " + fw_end)
    
    if ttl_flag:
        increase_inc_sales = True
    else:
        increase_inc_sales = False
    
    downstream_process(fpath_results=campaign_code + "/res_w_fb_weighted_" + campaign_code + "_" + fw + "_" + fw_end + "_7.csv",
                       BQ_output_suffix=campaign_code,
                       campaign_code=campaign_code,
                       fw=fw,
                       safari_run_date="2021-05-25",
                       create_dacamp_prod_mc_final=False,
                       have_historical_marketable_crn=True,
                       allocate_fb=True,
                       increase_inc_sales=increase_inc_sales,
                       fpath_comparison_results=campaign_code + "/comparison/MC_CRN",
                       bqclient=bqclient,
                       bqstorageclient=bqstorageclient,
                       division=division)[0]    

if __name__ == "__main__":
    # set user credentials again
    os.environ['GOOGLE_APPLICATION_CREDENTIALS']="adc.json"
    
    # grab creentials from default login, use gcloud auth login
    credentials, your_project_id = google.auth.default(
        scopes=["https://www.googleapis.com/auth/cloud-platform"]
    )
    
    # Make clients.
    bqclient = bigquery.Client(credentials=credentials, project='wx-bq-poc',)
    bqstorageclient = bigquery_storage.BigQueryReadClient(credentials=credentials)   
 
    # 1 command line argument - campaign ID, fw is the latest week
    if len(sys.argv) == 2:
        if sys.argv[1] == "test":
            import doctest
            pd.set_option('display.max_rows', 500)
            pd.set_option('display.max_columns', 500)
            pd.set_option('display.width', 1000)
            
            logging.info("Running doctests")
            doctest.testmod(optionflags=doctest.NORMALIZE_WHITESPACE)
            logging.info("Doctests complete")
            exit()
        else:
            campaign_code = sys.argv[1]
            fw = None
    # 2 command line arguments - specify both campaign code and fw
    elif len(sys.argv) == 3:  
        campaign_code = sys.argv[1]
        fw = sys.argv[2]    
    else:
        logging.error("invalid number of command line arguments. Usage: python run_FBAA_MC.py [campaign_code] [fw (optional)]")    

    run_downstream_process(campaign_code, fw, bqclient=bqclient, bqstorageclient=bqstorageclient)
