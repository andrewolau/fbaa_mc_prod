# -*- coding: utf-8 -*-
"""
Overview
    Runs post MTA downstream process - allocation to CRN level, application of business logic and
    upload to BQ
Usage
    Authenticate GCP account first:
        gcloud auth application-default login
        gcloud auth login
Author
    Andrew Lau
"""
from config import *
import pandas as pd
from matplotlib import pyplot as plt
import seaborn as sns
import datetime
import os
import google.auth
from google.cloud import bigquery
from google.cloud import bigquery_storage
from google.cloud.exceptions import NotFound
import sys
import email_notifications
import logging

def check_if_BQ_table_exists(table_id, bqclient=None, bqstorageclient=None):
    """
    Overview
        returns True or False depending on whether the table exists
    Arguments
        e.g. table_id = "wx-bq-poc.personal.AL_FBAA_MC_dacamp_prod_mc_final_crn_w_fb_2021-05-17_OSP-2373"
    """
    try:
        bqclient.get_table(table_id)  # Make an API request.
        logging.info("Table {} already exists.".format(table_id))
        return True
    except NotFound:
        return False

def update_mc_final_crn(campaign_code, dacamp_prod_mc_final, dacamp_prod_mc_final_to_insert, fw, bqclient=None, bqstorageclient=None):
    query_string = """
      -- delete old rows
    DELETE
    FROM
      {dacamp_prod_mc_final}
    WHERE
      campaign_code = "{campaign_code}";
      --   add new rows
    INSERT INTO
      {dacamp_prod_mc_final}
    SELECT
      *
    FROM
      {dacamp_prod_mc_final_to_insert}
    WHERE
      campaign_code = "{campaign_code}";
      
      -- CHECK
    WITH
      table_a AS (
      SELECT
        campaign_code,
        channel,
        medium,
        event,
        SUM(attributed_inc_sales) AS attributed_inc_sales,
        COUNT(*) AS count
      FROM
        {dacamp_prod_mc_final_to_insert}
      GROUP BY
        1,
        2,
        3,
        4 ),
      table_b AS (
      SELECT
        campaign_code,
        channel,
        medium,
        event,
        SUM(attributed_inc_sales) AS attributed_inc_sales,
        COUNT(*) AS count
      FROM
        `wx-bq-poc.wx_lty_digital_attribution_dev.AL_FBAA_MC_dacamp_prod_mc_final_crn_{fw}`
      WHERE
        campaign_code="{campaign_code}"
      GROUP BY
        1,
        2,
        3,
        4 )
    SELECT
      coalesce(a.campaign_code,
        b.campaign_code) as campaign_code,
      coalesce(a.channel,
        b.campaign_code) as channel,
      coalesce(a.medium,
        b.medium) as medium,
      coalesce(a.event,
        b.event) as event,
      a.attributed_inc_sales AS attributed_inc_sales_new,
      b.attributed_inc_sales AS attributed_inc_sales_old,
      a.count AS count_new,
      b.count AS count_old
    FROM
      table_a AS a
    FULL OUTER JOIN
      table_b AS b
    ON
      a.channel = b.channel
      AND a.medium = b.medium
      AND a.event = b.event;      
    """.format(campaign_code=campaign_code, dacamp_prod_mc_final=dacamp_prod_mc_final, dacamp_prod_mc_final_to_insert=dacamp_prod_mc_final_to_insert, fw=fw)
    df_bq = (
        bqclient.query(query_string)
        .result()
        .to_dataframe(bqstorage_client=bqstorageclient)
    )
    logging.info("SUCCESS... updated table " + dacamp_prod_mc_final + " using table " + dacamp_prod_mc_final_to_insert)
    return df_bq

def update_mc_final_crn_w_email(campaign_code, fw, send_email=True, bqclient=None, bqstorageclient=None):
    # only proceed if we have a backup of the table
    if check_if_BQ_table_exists("wx-bq-poc.wx_lty_digital_attribution_dev.AL_FBAA_MC_dacamp_prod_mc_final_crn_" + fw, bqclient=bqclient, bqstorageclient=bqstorageclient):
        results = update_mc_final_crn(campaign_code=campaign_code,
                                      dacamp_prod_mc_final="`gcp-wow-rwds-ai-safari-prod.digital_attribution_modelling.dacamp_prod_mc_final_crn`",
                                      dacamp_prod_mc_final_to_insert="`wx-bq-poc.personal.AL_FBAA_MC_dacamp_prod_mc_final_crn_w_fb_" + fw + "_" + campaign_code + "`",
                                      fw=fw,
                                      bqclient=bqclient,
                                      bqstorageclient=bqstorageclient)    
        logging.info(str(results))

        message_body = """
        Final table `gcp-wow-rwds-ai-safari-prod.digital_attribution_modelling.dacamp_prod_mc_final_crn` is ready for post-FBAA bit!

        Check out the Social Media Attribution dashboard for the results: https://datastudio.google.com/s/h3qJ85qpTjY

        Summary of changes to `gcp-wow-rwds-ai-safari-prod.digital_attribution_modelling.dacamp_prod_mc_final_crn`:
        {results}
        """.format(results=results)

        if send_email:
            email_recipient=EMAIL_RECIPIENT_ALL
        else:
            email_recipient=EMAIL_RECIPIENT

        email_notifications.send_email_notification(email_recipient=email_recipient,
                                        subject='wow.bot🤖: 🏆✔️ ALL FINISHED MTA with FB run for fw ' +  fw + ": " + campaign_code,
                                        message_body=message_body)
    else:
        logging.warning("backup does not exist... exiting")

if __name__ == "__main__":    
    # set user credentials again
    os.environ['GOOGLE_APPLICATION_CREDENTIALS']="adc.json" 

    # grab creentials from default login, use gcloud auth login
    credentials, your_project_id = google.auth.default(
        scopes=["https://www.googleapis.com/auth/cloud-platform"]
    )
    
    
    # Make clients.
    bqclient = bigquery.Client(credentials=credentials, project='wx-bq-poc',)
    bqstorageclient = bigquery_storage.BigQueryReadClient(credentials=credentials)

    # 1 command line argument - campaign ID, fw is the latest week
    if len(sys.argv) == 2:  
        campaign_code = sys.argv[1]
        today = datetime.date.today()
        last_monday = today + datetime.timedelta(days=-today.weekday(), weeks=-1)
        fw = str(last_monday)
    # 2 command line arguments - specify both campaign code and fw
    elif len(sys.argv) == 3:  
        campaign_code = sys.argv[1]
        fw = sys.argv[2]    
    else:
        logging.error("invalid number of command line arguments. Usage: python run_FBAA_MC.py [campaign_code] [fw (optional)]")
        
    update_mc_final_crn_w_email(campaign_code, fw, bqclient=bqclient, bqstorageclient=bqstorageclient)
