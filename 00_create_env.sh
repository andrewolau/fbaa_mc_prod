#!/bin/bash
# create the environment in the directory for easier access
conda create --prefix ./MTA_AL ipykernel -y
source activate ./MTA_AL

# forked repo with custom SQL scripts
python -m pip install git+https://andrewolau@bitbucket.org/andrewolau/mta.git

# install needed packages
conda install -c conda-forge seaborn pandas-gbq fsspec gcsfs 'google-cloud-bigquery[bqstorage,pandas]' google-api-python-client google-auth-httplib2 google-auth-oauthlib google-auth-oauthlib -y
yes | pip install google_spreadsheet