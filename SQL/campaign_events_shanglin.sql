-- suitable for 1 continuing time block
WITH
crn_d AS (
    SELECT DISTINCT
        crn,
        crn_enc AS h_crn
    FROM `wx-bq-poc.digital_attribution_modelling.tmp_customer_encrypted_detail`
    WHERE target_sys_name = 'ET'
        AND lylty_card_nbr_enc IS NOT NULL
)
,events AS
(
	SELECT
		-- time_utc as event_time
		UNIX_SECONDS(TIMESTAMP_TRUNC(time_utc, SECOND)) as event_time
		,CONCAT(channel_event, '_', campaign_start_date,'_', campaign_end_date)  as touch_point
		,crn
	from
	      `wx-bq-poc.wx_lty_digital_attribution_dev.AL_FBAA_MC_dacamp_prod_event_mw_{campaign_start_date_1}`
	WHERE
	 (
		campaign_code like '{campaign_code_1}%'
		AND
		(time_utc between '{campaign_start_date_1}' and '{campaign_end_date_1}'
		)
	)
)
SELECT
    crn_d.h_crn
	,touch_point as s0
	,NULL s1
	,NULL s2
	,NULL s3
	,NULL s4
	,NULL s5
	,NULL t1
	,NULL t2
	,NULL t3
	,NULL r1
	,NULL r2
	,NULL r3
	,event_time


FROM
    events
LEFT JOIN crn_d ON crn_d.crn = events.crn;
