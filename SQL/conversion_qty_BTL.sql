-- generate random encoding
CREATE OR REPLACE TABLE
  `wx-bq-poc.personal.AL_FBAA_MC_encoding_tmp` AS (
  SELECT
    25 * RAND() + 1 AS encoding_constant,
    CURRENT_DATETIME('Australia/Sydney') AS run_time_AEST,
    '{campaign_code_1}' AS campaign_code,
    '{campaign_start_date_1}' AS campaign_start_date );
-- save encoding
INSERT INTO
  `wx-bq-poc.personal.AL_FBAA_MC_encoding`
SELECT
  *
FROM
  `wx-bq-poc.personal.AL_FBAA_MC_encoding_tmp`;

WITH
crn_d AS (
    SELECT DISTINCT
        crn,
        crn_enc AS h_crn
    FROM `wx-bq-poc.digital_attribution_modelling.tmp_customer_encrypted_detail`
    WHERE target_sys_name = 'ET'
        AND lylty_card_nbr_enc IS NOT NULL
),
events AS (
  SELECT
    fw_start_date,
    crn,
    -- (case when inc_sales > 0 then 1 else 0 end) as conversion,
    -- (case when inc_sales > 0 then 0 else 1 end) as non_conversion,
    -- need to allow for edge cases where we can only match to CRNs with negative inc sales.
    -- allow conversion value to drive attribution
    1 as conversion,
    0 as non_conversion,    
    inc_sales as conversion_value
  from
   `wx-bq-poc.personal.AL_Safari_crn_level_game_theory_output_safari_full_promo_halo_post_current`
  where fw_start_date = '{campaign_start_date_1}' and campaign_code = '{campaign_code_1}' and pph = 'promo'
)

-- final output, list of CRNs and timestamp
SELECT
  crn_d.h_crn
  , NULL s0
	,NULL s1
	,NULL s2
	,NULL s3
	,NULL s4
	,NULL s5
	,NULL t1
	,NULL t2
	,NULL t3
    -- apply encoding
	,events.conversion_value * (SELECT encoding_constant FROM `wx-bq-poc.personal.AL_FBAA_MC_encoding_tmp`) r1
  -- pause encoding for time being
  -- ,events.conversion_value * 1 r1
	,events.conversion r2
	,events.non_conversion r3
  -- , CAST (fw_start_date as TIMESTAMP) as event_time
  -- UNIX_SECONDS(event_time)
	, UNIX_SECONDS(CAST (fw_start_date as TIMESTAMP)) as event_time


FROM events
LEFT JOIN crn_d ON crn_d.crn = events.crn
where crn_d.h_crn is not null
